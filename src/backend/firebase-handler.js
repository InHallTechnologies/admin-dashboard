import { initializeApp } from "firebase/app";
import { getDatabase } from 'firebase/database'
import { getAuth } from 'firebase/auth'
import { getStorage } from 'firebase/storage'
import { getAnalytics } from "firebase/analytics";

const firebaseConfig = {
  apiKey: "AIzaSyD6wJt5swBiIwJs0e1ANDuUlJ5DFYPDnQs",
  authDomain: "occuhire-tequed-labs.firebaseapp.com",
  databaseURL: "https://occuhire-tequed-labs-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "occuhire-tequed-labs",
  storageBucket: "occuhire-tequed-labs.appspot.com",
  messagingSenderId: "634670493520",
  appId: "1:634670493520:web:8f4ea347c7ef23419a8655",
  measurementId: "G-L3LMVQJPL4"
};

const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);

export const firebaseDatabase = getDatabase(app)
export const firebaseAuth = getAuth(app)
export const firebaseStorage = getStorage(app)
