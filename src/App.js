import './App.css';
import { Route, Routes } from 'react-router';
import LoginPage from './pages/login/login.component';
import { useEffect, useState } from 'react';
import { firebaseAuth, firebaseDatabase } from './backend/firebase-handler';
import { get, ref, child } from 'firebase/database'
import HomePage from './pages/home/home.component';
import JobDetailsPage from './pages/job-details/job-details.component';
import CompanyDetail from './pages/company-detail/company-detail.component';
import EditAssessmentPage from './pages/edit-assessment/edit-assessment.component';
import CollegeDetailPage from './pages/college-detail/college-detail.component';
import Applications from './pages/applications/applications.component';
import StudentDetail from './pages/student-detail/student-detail.component';
import OffersListPage from './pages/offers-list/offers-list.component';

function App() {

  const [currentState, setCurrentState] = useState("SPLASH")

  useEffect(() => {
    firebaseAuth.onAuthStateChanged((user) => {
      if (user) {
        if (user.email === "admin@abc.com") {
          setCurrentState("HOME")
        } else {
          setCurrentState("LOGIN")
        }
      } else {
        setCurrentState("LOGIN")
      }
    })
  }, [])


  if (currentState === "SPLASH") {
    return(
      <div></div>
    )
  }

  if (currentState === "LOGIN") {
    return(
      <LoginPage setCurrentState={setCurrentState} />
    )
  }

  return (
    <Routes>
      <Route path="/" element={<HomePage />} />
      <Route path="/job-detail" element={<JobDetailsPage />} />
      <Route path="/company-detail" element={<CompanyDetail/>} />
      <Route path="/edit-assessment" element={<EditAssessmentPage/>} />
      <Route path="/college-detail" element={<CollegeDetailPage/>} />
      <Route path="/applications" element={<Applications/>} />
      <Route path="/student-detail" element={<StudentDetail/>} />
      <Route path="offers-list" element={<OffersListPage/>} />
    </Routes>
  );
}

export default App