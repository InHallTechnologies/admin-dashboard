import React, { useState } from "react";
import { Button, Form, Table, Image, Spinner, Modal } from "react-bootstrap";
import { useLocation, useNavigate } from "react-router-dom";
import logo from '../../assets/logo.png'
import assessmentSample from "../../entities/assessment-sample";
import './edit-assessment.style.scss'
import 'react-toastify/dist/ReactToastify.css';
import { toast, ToastContainer } from "react-toastify";
import { push, get, ref, set, remove } from 'firebase/database'
import { firebaseDatabase, firebaseStorage } from "../../backend/firebase-handler";
import { ref as firebaseStorageRef, uploadBytesResumable, getDownloadURL } from 'firebase/storage';
import { IoMdArrowRoundBack } from 'react-icons/io'

const EditAssessmentPage = () => {

    const location = useLocation()
    const type = location.state.type
    const [detail, setDetail] = useState(location.state.item?location.state.item:assessmentSample)
    const [loading, setLoading] = useState(false)
    const [tempQues, setTempQues] = useState({question:"", option1:"", option2:"", option3:"", option4:"", rightAnswere:""})
    const [uploadingImage, setUploadingImage] = useState(false);  
    const [uploadPercentage, setUploadPercentage] = useState(0);
    const [deleteModal, setDeleteModal] = useState(false)
    const navigate = useNavigate()

    const handleAdd = () => {
        if (tempQues.question === "") {
            toast.warn("Please enter the question")
            return
        }
        if (tempQues.option1 === "" || tempQues.option2 === "" || tempQues.option3 === "" || tempQues.option4 === "") {
            toast.warn("Please enter all the four options")
            return
        }
        if (tempQues.rightAnswere === "") {
            toast.warn("Please select the right answer")
            return
        }
        var tempArray = detail.questions?detail.questions:[]
        tempArray.push(tempQues)
        setDetail({...detail, questions:tempArray})
        setTempQues({question:"", option1:"", option2:"", option3:"", option4:"", rightAnswere:""})
        document.getElementById("a").checked = false
        document.getElementById("b").checked = false
        document.getElementById("c").checked = false
        document.getElementById("d").checked = false
    }

    const handleLogoChange = () => {
        const inputElement = document.createElement('input');
        inputElement.setAttribute('type',"file");
        inputElement.setAttribute('accept','image/*');
        inputElement.onchange = (event) => {
            const files = event.target.files;
            if (detail.id === "") {
                detail.id = push(ref(firebaseDatabase, "ASSESSMENT_DETAILS")).key
            }
            const logoRef = firebaseStorageRef(firebaseStorage, "COURSE_LOGO/"+detail.id);            
            const uploadTask = uploadBytesResumable(logoRef,files[0], {contentType:'image/png'}); 
            uploadTask.on('state_changed', (snapshot) => {
                setUploadingImage(true);
                const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                setUploadPercentage(progress.toFixed(2));
            }, () => {
                toast.error('Something went wrong');
                setUploadingImage(false);
            }, async () => {
                const url = await getDownloadURL(logoRef);
                setDetail({...detail, courseLogo:url});
                setUploadingImage(false);
            })

        }
        inputElement.click();

    }

    const handleSubmit = async () => {
        if (detail.courseName === "") {
            toast.warn("Please enter the course name")
            return
        }
        if (detail.totalScore === "") {
            toast.warn("Please enter the total score")
            return
        }
        if (detail.questions.length === 0) {
            toast.warn("Please add at least one question")
            return
        }
        if (detail.id === "" || detail.id === undefined) {
            detail.id = push(ref(firebaseDatabase, "ASSESSMENT_DETAILS")).key
        }
        setLoading(true)
        var date = new Date().getDate(); 
        var month = new Date().getMonth() + 1; 
        var year = new Date().getFullYear(); 
        var hours = new Date().getHours(); 
        var min = new Date().getMinutes();
        var sec = new Date().getSeconds(); 
        detail.postingDate = date + "-" + month + "-" + year + " " + hours + ":" + min + ":" + sec
        if (type === "Pre Qualifying Exams") {
            await set(ref(firebaseDatabase, "PREQUALIFYING_EXAMS/"+detail.id), detail)
        } else {
            await set(ref(firebaseDatabase, "ASSESSMENT_DETAILS/"+detail.id), detail)
        }
        toast.success("Course saved.")
        setLoading(false)
    }

    const handleDelete = async () => {
        setLoading(true)

        await set(ref(firebaseDatabase, "DELETED_TESTS/"+detail.id), detail)
        if (type === "Pre Qualifying Exams") {
            await remove(ref(firebaseDatabase, "PREQUALIFYING_EXAMS/"+detail.id))
        } else {
            await remove(ref(firebaseDatabase, "ASSESSMENT_DETAILS/"+detail.id))
        }

        toast.success("Test Deleted")
        const timer = setTimeout(() => {
            navigate(-1)
        }, 1500);
        return () => clearTimeout(timer);
    }

    return(
        <div className="edit-assessment-container">
            <div className="back-logo-container">
                <IoMdArrowRoundBack  size={20}  style={{cursor:"pointer"}} onClick={()=>{navigate(-1)}} />
                <img src={logo} alt="OccuHire" className="logo" />
            </div>  

            <div className="sub-container">
                <p className="section-title">"Add/Edit {type} Details"</p>

                {
                    uploadingImage
                    ?
                    <div className='profile-logo' >
                        <Spinner animation="border" size={20} /> 
                        <span>{uploadPercentage}% Uploaded</span> 
                    </div>
                    :
                    <Image src={detail.courseLogo?detail.courseLogo:"https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COURSE_LOGO%2Fcoding.png?alt=media&token=8eb517b9-b3ce-4e03-b094-e48f789fc9a4"} onClick={handleLogoChange} className='profile-logo' roundedCircle />
                }

                <Form className="form-detail-container">
                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Course Name</Form.Label>
                        <Form.Control className="field" required value={detail.courseName} onChange={(event)=>{setDetail({...detail, courseName:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Total Score</Form.Label>
                        <Form.Control className="field" required value={detail.totalScore} onChange={(event)=>{setDetail({...detail, totalScore:event.target.value.replace(/[^0-9]/g, '')})}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Duration (in minutes)</Form.Label>
                        <Form.Control className="field" required value={detail.duration} onChange={(event)=>{setDetail({...detail, duration:event.target.value.replace(/[^0-9]/g, '')})}} />
                    </Form.Group>

                    <div className="ete-field subtitle-container">
                        <p className="subtitle">Questions/Option/Answers (Click "Add" to save detail)</p>
                        <p className="add-button" onClick={handleAdd}>Add</p>
                    </div>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Question</Form.Label>
                        <Form.Control className="field" as="textarea" rows={10} required value={tempQues.question} onChange={(event)=>{setTempQues({...tempQues, question:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Option 1</Form.Label>
                        <Form.Control className="field" required value={tempQues.option1} onChange={(event)=>{setTempQues({...tempQues, option1:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Option 2</Form.Label>
                        <Form.Control className="field" required value={tempQues.option2} onChange={(event)=>{setTempQues({...tempQues, option2:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Option 3</Form.Label>
                        <Form.Control className="field" required value={tempQues.option3} onChange={(event)=>{setTempQues({...tempQues, option3:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Option 4</Form.Label>
                        <Form.Control className="field" required value={tempQues.option4} onChange={(event)=>{setTempQues({...tempQues, option4:event.target.value})}} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Right Answer</Form.Label>
                        <div className="ete-field">
                            <Form.Check inline name="group1" type='radio' label={"Option 1"} value={"Option 1"} id="a" onChange={(event)=>{setTempQues({...tempQues, rightAnswere:tempQues.option1})}} />   
                            <Form.Check inline name="group1" type='radio' label={"Option 2"} value={"Option 2"} id="b" onChange={(event)=>{setTempQues({...tempQues, rightAnswere:tempQues.option2})}} /> 
                            <Form.Check inline name="group1" type='radio' label={"Option 3"} value={"Option 3"} id="c" onChange={(event)=>{setTempQues({...tempQues, rightAnswere:tempQues.option3})}} /> 
                            <Form.Check inline name="group1" type='radio' label={"Option 4"} value={"Option 4"} id="d" onChange={(event)=>{setTempQues({...tempQues, rightAnswere:tempQues.option4})}} /> 
                        </div>
                        </Form.Group>
                </Form>

                <div className="ete-field" ><Table striped hover className="ete-field" responsive style={{marginTop:20}}>
                            <thead>
                                <tr>
                                    <th>Q.No.</th>
                                    <th>Question</th>
                                    <th>Op1</th>
                                    <th>Op2</th>
                                    <th>Op3</th>
                                    <th>Op4</th>
                                    <th>Right</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    detail.questions.map((item, index)=>{return(
                                        <tr>
                                            <td>{index+1}</td>
                                            <td>{item.question}</td>
                                            <td>{item.option1}</td>
                                            <td>{item.option2}</td>
                                            <td>{item.option3}</td>
                                            <td>{item.option4}</td>
                                            <td>{item.rightAnswere}</td>
                                            <td style={{cursor:"pointer", fontStyle:"italic", color:"red"}} onClick={()=>{const newFilter = detail.questions.filter((currentItem) => currentItem !== item)
                                                setDetail({...detail, questions:newFilter})}}>Remove</td>
                                        </tr>
                                    )})
                                }
                            </tbody>
                </Table></div>

                <div className='button-container'>
                    <Button variant="primary" disabled={loading} className="button" onClick={handleSubmit} >
                        Save
                    </Button>


                    {
                        location.state.item
                        &&
                        <Button variant="secondary" disabled={loading} className='delete-button' type="submit" onClick={()=>{setDeleteModal(true)}}>
                            Delete Test
                        </Button>
                    }
                </div>

            </div>

            <ToastContainer
                position="top-right"
                autoClose={2000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                />
            <ToastContainer />

            <Modal show={deleteModal} onHide={()=>{setDeleteModal(false)}}>
                <Modal.Header closeButton>
                    <Modal.Title>Confirm!</Modal.Title>
                </Modal.Header>
                
                <Modal.Body>
                    <p>Are you sure you want to permanently delete this test?</p>
                </Modal.Body>
                
                <Modal.Footer>
                    <Button variant="secondary" onClick={()=>{setDeleteModal(false)}}>Cancel</Button>
                    <Button variant="primary" onClick={()=>{handleDelete(); setDeleteModal(false)}}>Delete</Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}

export default EditAssessmentPage