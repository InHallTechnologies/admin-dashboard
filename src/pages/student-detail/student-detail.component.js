import React, { useState} from "react";
import { Button, Form, Modal, Table } from "react-bootstrap";
import './student-detail.style.scss'
import 'react-toastify/dist/ReactToastify.css';
import { toast, ToastContainer } from "react-toastify";
import { useLocation, useNavigate } from "react-router-dom";
import { set, get, ref, child } from 'firebase/database'
import { firebaseDatabase } from "../../backend/firebase-handler";
import logo from '../../assets/logo.png'
import { IoMdArrowRoundBack } from 'react-icons/io'
import { FiDownload } from 'react-icons/fi'

const StudentDetail = () => {

    const location = useLocation()
    const userProfile = location.state.item
    const [contentLoading, setContentLoading] = useState(true)
    const jobId = location.state.jobId
    const companyId = location.state.companyId
    const [loading, setLoading] = useState(false)
    const navigate = useNavigate()
    const [modalVisibility, setModalVisibility] = useState(false)
    const [offeredPackage, setPackage] = useState("")

    useState(() => {
        setContentLoading(false)
    }, [])

    const handleButton = async (item, type) => {
        item.status = type
        setLoading(true)
        await set(ref(firebaseDatabase, "COMPANY_APPLICANTS_LIST/"+companyId+"/"+jobId+"/"+item.key+"/status"), type)
        await set(ref(firebaseDatabase, "APPLICATION_STATUS/"+item.key+"/"+jobId+"/status"), type)
        if (type === "Placed") {
            await get(child(ref(firebaseDatabase), "STUDENTS_ARCHIVE/"+item.department+"/"+item.key+"/placed")).then(async(snap)=>{
                let array = []
                if(snap.exists()) {
                    array = snap.val()
                    let tempObj = {id:jobId, package:offeredPackage}
                    array.push(tempObj)
                } else {

                    let tempObj = {id:jobId, package:offeredPackage}
                    console.log("obj " + tempObj)
                    array.push(tempObj)
                }
                await set(ref(firebaseDatabase, "STUDENTS_ARCHIVE/"+item.department+"/"+item.key+"/placed"), array)
                await set(ref(firebaseDatabase, "APPLICATION_STATUS/"+item.key+"/"+jobId+"/package"), offeredPackage)
                await set(ref(firebaseDatabase, "COMPANY_APPLICANTS_LIST/"+companyId+"/"+jobId+"/"+item.key+"/package"), offeredPackage)
            })
            if (userProfile.accountType === "LINKED") {
                await get(child(ref(firebaseDatabase), "COLLEGE_WISE_STUDENTS/"+item.collegeUid+"/"+item.department+"/"+item.collegeKey+"/placed")).then(async(snap)=>{
                    let array = []
                    if(snap.exists()) {
                        array.push(snap.val())
                        array.push(jobId)
                    } else {
                        array.push(jobId)
                    }
                    await set(ref(firebaseDatabase, "COLLEGE_WISE_STUDENTS/"+item.collegeUid+"/"+item.department+"/"+item.collegeKey+"/placed"), array)
                    await set(ref(firebaseDatabase, "COLLEGE_APPLICANTS_LIST/"+item.collegeUid+"/"+companyId+"/"+jobId+"/"+item.key+"/package"), offeredPackage)
                    // await get(child(ref(firebaseDatabase), "COLLEGE_PACKAGE_STAT/"+item.collegeUid)).then((snap)=>{
                    //     if(snap.child(item.department).exists()) {
                    //         let max = parseInt(snap.child(item.department).child("highest").val())
                    //         let number = parseInt(snap.child(item.department).child("number").val())
                    //         let average = parseInt(snap.child(item.department).child("average").val())
                    //         if (parseInt(offeredPackage) > max) {
                    //             let obj = {highest:offeredPackage, number:}
                    //         }
                    //     }
                    // })
                })
            }
        }
        toast.success("Student " + type)
    }

    const handleDownloadResume = async () => {
        window.open(`https://resume-creator-occuhite.vercel.app/${userProfile.department}/${userProfile.uid}`)
    }

    return(
        <div className="manage-resume-container">

            <div className="back-logo-container">
                <IoMdArrowRoundBack  size={20}  style={{cursor:"pointer"}} onClick={()=>{navigate(-1)}} />
                <img src={logo} alt="OccuHire" className="logo" />
            </div>
            
            {
                contentLoading
                ?
                <div></div>
                :
            
            <div className="download-form-container">
                <div className="download-container" onClick={handleDownloadResume} >
                    <FiDownload size={20} color="#3761EE" />
                    <p className="download-tag">Download Resume</p>
                </div>

                <Form className="resume-form-container">

                    <p className="ete-field section-title">Personal Details</p>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1" >
                        <Form.Label className="tag">Full Name</Form.Label>
                        <Form.Control className="field" disabled type="text" value={userProfile.name} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1" >
                        <Form.Label className="tag">USN</Form.Label>
                        <Form.Control className="field" disabled type="text" value={userProfile.usn} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Phone Number</Form.Label>
                        <Form.Control maxLength={10} disabled className="field" type="number-pad" value={userProfile.phoneNumber} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1" >
                        <Form.Label className="tag">Email</Form.Label>
                        <Form.Control className="field" disabled type="text" value={userProfile.emailId} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">College Name</Form.Label>
                        <Form.Control disabled className="field" value={userProfile.collegeName} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Department</Form.Label>
                        <Form.Control className="field" value={userProfile.department} disabled />             
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Semester</Form.Label>
                        <Form.Control disabled className="field" type="text" value={userProfile.semester} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Gender</Form.Label>
                        <Form.Control disabled className="field" type="text" value={userProfile.gender} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Year of Study</Form.Label>
                        <Form.Control disabled className="field" type="text" value={userProfile.yearOfStudy} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Date of Birth</Form.Label>
                        <Form.Control disabled className="field" type="date" value={userProfile.dateOfBirth} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Aadhar Number</Form.Label>
                        <Form.Control disabled className="field" type="text" value={userProfile.aadharNumber} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">LinkedIn Profile URL</Form.Label>
                        <Form.Control disabled className="field" value={userProfile.linkedInUrl} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Address</Form.Label>
                        <Form.Control disabled className="field" value={userProfile.address} />
                    </Form.Group>

                    <p className="section-title">Education Details</p>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">10th Standard School</Form.Label>
                        <Form.Control disabled className="field" value={userProfile.tenthSchool} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">10th Standard Board</Form.Label>
                        <Form.Control disabled className="field" type="text" value={userProfile.tenthBoard} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">10th Standard Percentage</Form.Label>
                        <Form.Control disabled className="field" type="text" value={userProfile.tenthPercentage} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">12th Standard School</Form.Label>
                        <Form.Control disabled className="field" value={userProfile.twelthSchool} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">12th Standard Board</Form.Label>
                        <Form.Control disabled className="field" type="text" value={userProfile.twelthBoard}  />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">12th Standard Percentage</Form.Label>
                        <Form.Control disabled className="field" type="text" value={userProfile.twelthPercentage} />
                    </Form.Group>

                    <Form.Group className="left-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Current Engineering CGPA</Form.Label>
                        <Form.Control disabled className="field" type="text" value={userProfile.cgpa} />
                    </Form.Group>

                    <Form.Group className="right-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">No. of Active Backlogs</Form.Label>
                        <Form.Control disabled className="field" type="number-pad" value={userProfile.backlogs}  />
                    </Form.Group>

                    <p className="section-title">Technical Skills Details</p>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Programming languages known</Form.Label>
                        <Form.Control disabled className="field" value={userProfile.languagesKnown} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Technologies prevalent in</Form.Label>
                        <Form.Control disabled className="field" value={userProfile.prevalentTechnologies} />
                    </Form.Group>

                    <div className="ete-field subtitle-container">
                        <p className="subtitle">Project Details</p>
                    </div>
                    {
                        (userProfile.projectDetails && userProfile.projectDetails.length !== 0)
                        &&
                        <div className="ete-field"><Table striped hover className="ete-field" responsive>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    userProfile.projectDetails.map((item, index)=>{return(
                                        <tr>
                                            <td>{index+1}</td>
                                            <td>{item.name}</td>
                                            <td>{item.description}</td>
                                        </tr>
                                    )})
                                }
                            </tbody>
                        </Table></div>
                    }

                    <div className="ete-field subtitle-container">
                        <p className="subtitle">Internship Details</p>
                    </div>

                    {
                        (userProfile.internshipDetails && userProfile.internshipDetails.length !== 0)
                        &&
                        <div className="ete-field"><Table striped hover className="ete-field" responsive>
                            <thead>
                                <tr>
                                    <th>Company Name</th>
                                    <th>Role</th>
                                    <th>Duration</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    userProfile.internshipDetails.map((item, index)=>{return(
                                        <tr>
                                            <td>{item.compName}</td>
                                            <td>{item.role}</td>
                                            <td>{item.duration}</td>
                                            <td>{item.startDate}</td>
                                            <td>{item.endDate}</td>
                                            <td>{item.description}</td>
                                        </tr>
                                    )})
                                }
                            </tbody>
                        </Table></div>
                    }

                    <div className="ete-field subtitle-container">
                        <p className="subtitle">Certificate Details</p>
                    </div>

                    {
                        (userProfile.certificationDetails && userProfile.certificationDetails.length !== 0)
                        &&
                        <div className="ete-field"><Table striped hover className="ete-field" responsive>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Certificate Name</th>
                                    <th>Date of Completion</th>
                                    <th>Issuing Agency</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    userProfile.certificationDetails.map((item, index)=>{return(
                                        <tr>
                                            <td>{index+1}</td>
                                            <td>{item.name}</td>
                                            <td>{item.date}</td>
                                            <td>{item.agency}</td>
                                        </tr>
                                    )})
                                }
                            </tbody>
                        </Table></div>
                    }

                    <div className="ete-field subtitle-container">
                        <p className="subtitle">Research paper publications</p>
                    </div>

                    {
                        (userProfile.researchPapers && userProfile.researchPapers.length !== 0)
                        &&
                        <div className="ete-field"><Table striped hover className="ete-field" responsive>
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Agency</th>
                                    <th>Date</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    userProfile.researchPapers.map((item, index)=>{return(
                                        <tr>
                                            <td>{item.title}</td>
                                            <td>{item.agency}</td>
                                            <td>{item.date}</td>
                                            <td>{item.description}</td>
                                        </tr>
                                    )})
                                }
                            </tbody>
                        </Table></div>
                    }

                    <div className="ete-field subtitle-container">
                        <p className="subtitle">Awards and Achievements</p>
                    </div>
                    
                    {
                        (userProfile.awards && userProfile.awards.length !== 0)
                        &&
                        <div className="ete-field"><Table striped hover className="ete-field" responsive>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Date</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    userProfile.awards.map((item, index)=>{return(
                                        <tr>
                                            <td>{index+1}</td>
                                            <td>{item.name}</td>
                                            <td>{item.date}</td>
                                            <td>{item.description}</td>
                                        </tr>
                                    )})
                                }
                            </tbody>
                        </Table></div>
                    }

                    <div className="ete-field subtitle-container">
                        <p className="subtitle">Additional Details</p>
                    </div>

                    {
                        (userProfile.additionalDetails && userProfile.additionalDetails.length !== 0)
                        &&
                        <div className="ete-field"><Table striped hover className="ete-field" responsive>
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    userProfile.additionalDetails.map((item, index)=>{return(
                                        <tr>
                                            <td>{index+1}</td>
                                            <td>{item.name}</td>
                                            <td>{item.description}</td>
                                        </tr>
                                    )})
                                }
                            </tbody>
                        </Table></div>
                    }

                    <p className="section-title">More about yourself</p>
                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Programming Proficiency</Form.Label>
                        <Form.Control disabled className="field" value={userProfile.proficiency} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Expected Salary Package</Form.Label>
                        <Form.Control disabled className="field" value={userProfile.expectedPackage} />
                    </Form.Group>

                    <Form.Group className="ete-field field-container" controlId="exampleForm.ControlInput1" >
                        <Form.Label className="tag">Interested Domains</Form.Label>
                        <div className="ete-field">
                            <Form.Check inline name="group3" type='checkbox' label={"Developer (C, C++, Java, Python)"} value={"Developer (C, C++, Java, Python"} checked={userProfile.interestedDomain.developer} disabled />   
                            <Form.Check inline name="group3" type='checkbox' label={"Software Testing"} value={"Software Testing"} checked={userProfile.interestedDomain.testing} disabled /> 
                            <Form.Check inline name="group3" type='checkbox' label={"Analyst"} value={"Analyst"} checked={userProfile.interestedDomain.analyst} disabled /> 
                            <Form.Check inline name="group3" type='checkbox' label={"Data Scientist"} value={"Data Scientist"} checked={userProfile.interestedDomain.dataScientist} disabled  /> 
                            <Form.Check inline name="group3" type='checkbox' label={"Dev-Ops Engineer"} value={"Dev-Ops Engineer"} checked={userProfile.interestedDomain.devOps} />
                            <Form.Check inline name="group3" type='checkbox' label={"Full Stack Web Developer"} value={"Full Stack Web Developer"} checked={userProfile.interestedDomain.fullStackWeb} disabled /> 
                            <Form.Check inline name="group3" type='checkbox' label={"Marketing Roles"} value={"Marketing Roles"} checked={userProfile.interestedDomain.marketing} disabled /> 
                            <Form.Check inline name="group3" type='checkbox' label={"Business Development"} value={"Business Development"} checked={userProfile.interestedDomain.businessDev} disabled /> 
                            <Form.Check inline name="group3" type='checkbox' label={"I am not sure. Open for any role."} value={"I am not sure. Open for any role."} checked={userProfile.interestedDomain.notsure} disabled /> 
                        </div>
                    </Form.Group>
                    {
                        (userProfile.preQualResult && userProfile.preQualResult !== undefined)
                        &&
                        <div className="ete-field">
                        <div className="ete-field subtitle-container">
                            <p className="subtitle">Pre-qualifying exam</p>
                        </div>
                        <Table striped hover className="ete-field">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Course Name</th>
                                    <th>Percentage</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    userProfile.preQualResult.map((item, index)=>{return(
                                        <tr>
                                            <td>{index+1}</td>
                                            <td>{item.name}</td>
                                            <td>{item.score}</td>
                                        </tr>
                                    )})
                                }
                            </tbody>
                        </Table>
                        </div>
                    }
                    {
                        (userProfile.skillResult && userProfile.skillResult !== undefined)
                        &&
                        <div className="ete-field">
                        <div className="ete-field subtitle-container">
                            <p className="subtitle">Skill assessments</p>
                        </div>
                        <Table striped hover className="ete-field">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Course Name</th>
                                    <th>Percentage</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    userProfile.skillResult.map((item, index)=>{return(
                                        <tr>
                                            <td>{index+1}</td>
                                            <td>{item.name}</td>
                                            <td>{item.score}</td>
                                        </tr>
                                    )})
                                }
                            </tbody>
                        </Table>
                        </div>
                    }
                </Form>

                <div style={{display:"flex", flexDirection:"row", alignItems:"center", justifyContent:"center", width:"100%"}}>
                    <Button className="upgrade-button-secondary" disabled={loading} variant="primary" onClick={()=>{handleButton(userProfile, "Rejected")}}>Reject</Button>
                    <Button className="upgrade-button" disabled={loading} variant="primary" onClick={()=>{setModalVisibility(true)}}>Accept</Button>
                </div>
            </div>
            }

            <ToastContainer
                position="top-right"
                autoClose={2000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                />
            <ToastContainer />

            <Modal show={modalVisibility} onHide={()=>{setModalVisibility(false)}}>
                <Modal.Header closeButton >
                  <Modal.Title>Offered package</Modal.Title>
                </Modal.Header>
                                
                <Modal.Body>
                  <p>Please enter the offered package amount.</p>
                  <input placeholder="₹ X,XX,XXX" value={offeredPackage} onChange={(event)=>{setPackage(event.target.value.replace(/[^0-9]/g, ''))}} />
                </Modal.Body>
                                
                <Modal.Footer>
                  <Button variant="secondary" onClick={()=>{setModalVisibility(false)}}>Close</Button>
                  <Button variant="primary" onClick={()=>{handleButton(userProfile, "Placed")}}>Submit</Button>
                </Modal.Footer>
            </Modal>
            
        </div>
    )
}

export default StudentDetail