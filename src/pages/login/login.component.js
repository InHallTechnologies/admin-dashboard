import React, { useEffect, useState } from "react";
import './login.style.scss'
import logo from '../../assets/logo.png'
import { Form, Spinner, Button } from "react-bootstrap";
import { signInWithEmailAndPassword } from 'firebase/auth'
import { firebaseAuth } from "../../backend/firebase-handler";
import { toast, ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

const LoginPage = ({setCurrentState}) => {

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [currentError, setCurrentError] = useState("")
    const [loading, setLoading] = useState(false)

    const handleLogin = () => {
        if (email === "") {
            setCurrentError("email")
            return
        }
        if (password === "") {
            setCurrentError("password")
            return
        }
        setCurrentError("")
        setLoading(true)
        
        signInWithEmailAndPassword(firebaseAuth, email, password).then((credential) => {
            toast.success("Login successful.")
            setLoading(false)
            setCurrentState("HOME")
        })
        .catch((error) => {
            toast.warn("Invalid credentials.")
            setLoading(false)
        })
    }

    return(
        <div className="login-page-container">
            <img src={logo} alt="OccuHire" className="logo" />
            <div className="login-form-container">
                <p className="welcome-tag">Welcome to OccuHire-Admin</p>
                <p className="sub-tag">Please login to continue</p>

                <Form className="form-container">
                    <Form.Group className="field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Email address</Form.Label>
                        <Form.Control className="field" type="email" placeholder="name@example.com" value={email} onChange={(event)=>{setEmail(event.target.value)}} />
                        <Form.Text style={currentError==="email"?{}:{display:"none"}} className="error-tag">Please enter your email-id</Form.Text>
                    </Form.Group>
                    <Form.Group className="field-container" controlId="exampleForm.ControlInput1">
                        <Form.Label className="tag">Password</Form.Label>
                        <Form.Control className="field" type="email" placeholder="* * * * * * * *" value={password} onChange={(event)=>{setPassword(event.target.value)}} />
                        <Form.Text style={currentError==="password"?{}:{display:"none"}} className="error-tag">Please enter a password with at least 8 characters</Form.Text>
                    </Form.Group>
                </Form>

                <div className="buttons-container">
                    <Button variant="primary" disabled={loading} className="login-button" onClick={handleLogin}>
                        {
                            loading
                            ?
                            <Spinner animation="border" variant="light" />
                            :
                            "Login"
                        }
                    </Button>
                    {/* <p className="secondary-button">Forgot Password?</p> */}
                </div>
            </div>

            <ToastContainer
                position="top-right"
                autoClose={2000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                />
            <ToastContainer />
        </div>
    )
}

export default LoginPage