import React, { useState, useEffect } from 'react';
import './company-detail.style.scss'
import { Form, Button, Image, Spinner, Modal } from 'react-bootstrap';
import { firebaseAuth, firebaseDatabase, firebaseStorage } from '../../backend/firebase-handler';
import { get, ref, set, push, remove } from 'firebase/database';
import { ToastContainer, toast } from "react-toastify";
import { ref as firebaseStorageRef, uploadBytesResumable, getDownloadURL } from 'firebase/storage';
import { useLocation, useNavigate } from 'react-router-dom';
import Header from '../../components/header/header.component';
import logo from '../../assets/logo.png'
import { IoMdArrowRoundBack } from 'react-icons/io'


const CompanyDetail = () => {

    const location = useLocation()
    const [credentials, setCredentials] = useState(location.state.item?location.state.item:{companyLogo:"https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fcompany.png?alt=media&token=5e1ff6bf-3e02-46b1-b254-192387511f24"});
    const [title, setTitle] = useState(location.state.type==="VIEW"?"Company Detail":"Add Company")
    const [uploadingImage, setUploadingImage] = useState(false);  
    const [uploadPercentage, setUploadPercentage] = useState(0);
    const [loading, setLoading] = useState(false)
    const navigate = useNavigate()
    const [deleteModal, setDeleteModal] = useState(false)

    useEffect(() => {
       
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();
        setLoading(true)
        if (location.state.type === "VIEW") {
            if (credentials.hrContactNumber.length > 0 && credentials.hrContactNumber.length < 10) {
                toast.warn("Please enter a valid HR Phone Number")
                setLoading(false)
                return
            }
            const profileRef = ref(firebaseDatabase, `COMPANY_ARCHIVE/`+credentials.companyUID);
            await set(profileRef, {...credentials});
            toast.success(`Saved Changes`);
            setLoading(false)
        } else {
            console.log(credentials.hrContactNumber)
            if (credentials.hrContactNumber && credentials.hrContactNumber.length === 10) {
                toast.warn("Please enter a valid HR Phone Number")
                setLoading(false)
                return
            }
            var date = new Date().getDate(); 
            var month = new Date().getMonth() + 1; 
            var year = new Date().getFullYear(); 
            var hours = new Date().getHours(); 
            var min = new Date().getMinutes();
            var sec = new Date().getSeconds(); 
            credentials.date = date + "-" + month + "-" + year + " " + hours + ":" + min + ":" + sec
            credentials.author = "ADMIN"
            if (credentials.companyUID === "" || credentials.companyUID === undefined) {
                credentials.companyUID = push(ref(firebaseDatabase, "COMPANY_ARCHIVE")).key
            }
            await set(ref(firebaseDatabase, "COMPANY_ARCHIVE/"+credentials.companyUID), credentials);
            toast.success("Company added");
            setLoading(false)
            setCredentials({companyName:"", companyAddress:"", email:"", companyUID:"", hrName:"", hrContactNumber:"", companyLogo:"https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fcompany.png?alt=media&token=5e1ff6bf-3e02-46b1-b254-192387511f24"})

            const timer = setTimeout(() => {
                navigate(-1)
            }, 1500);
            return () => clearTimeout(timer);
        }
    }

    const handleLogoChange = () => {
        const inputElement = document.createElement('input');
        inputElement.setAttribute('type',"file");
        inputElement.setAttribute('accept','image/*');
        inputElement.onchange = (event) => {
            const files = event.target.files;
            let id = ""
            if (location.state.type==="VIEW") {
                id = credentials.companyUID
            } else {
                id = push(ref(firebaseDatabase, "COMPANY_ARCHIVE")).key
                credentials.companyUID = id
            }
            console.log(id)
            const logoRef = firebaseStorageRef(firebaseStorage, "COMPANY_LOGO/"+id+"/logo");            
            const uploadTask = uploadBytesResumable(logoRef,files[0], {contentType:'image/png'}); 
            uploadTask.on('state_changed', (snapshot) => {
                setUploadingImage(true);
                const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                setUploadPercentage(progress.toFixed(2));
            }, () => {
                toast.error('Something went wrong');
                setUploadingImage(false);
            }, async () => {
                const url = await getDownloadURL(logoRef);
                setCredentials({...credentials, companyLogo:url});
                setUploadingImage(false);
            })

        }
        inputElement.click();

    }

    const handleDelete = async () => {
        setLoading(true)

        await set(ref(firebaseDatabase, "DELETED_COMPANIES/"+credentials.companyUID), credentials)
        await remove(ref(firebaseDatabase, "COMPANY_ARCHIVE/"+credentials.companyUID))

        toast.success("Company Deleted")
        const timer = setTimeout(() => {
            navigate(-1)
        }, 1500);
        return () => clearTimeout(timer);
    }

    return(
        <div className='company-profile-container'>

            <div className="back-logo-container">
                <IoMdArrowRoundBack  size={20}  style={{cursor:"pointer"}} onClick={()=>{navigate(-1)}} />
                <img src={logo} alt="OccuHire" className="logo" />
            </div>    
                    
            <div className='company-profile-content'>
                <h2 className='section-title' >{title}</h2>
                
                {
                    uploadingImage
                    ?
                    <div className='profile-logo' >
                        <Spinner animation="border" size={20} /> 
                        <span>{uploadPercentage}% Uploaded</span> 
                    </div>
                    :
                    <Image src={credentials.companyLogo?credentials.companyLogo:"https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fuser.png?alt=media&token=10c0d83b-1a74-46a1-8d18-72dbb7b04cde"} onClick={handleLogoChange} className='profile-logo' roundedCircle />
                }
                <Form className='main-form' onSubmit={handleSubmit} >
                    <Form.Group  className="mb-3 first-half">
                        <Form.Label className='text-label '>Company Name</Form.Label>
                        <Form.Control size='lg' required className='text-input' value={credentials.companyName} onChange={(event)=>{setCredentials({...credentials, companyName:event.target.value})}} />
                    </Form.Group>

                    <Form.Group  className="mb-3 second-half">
                        <Form.Label className='text-label'>Email Id</Form.Label>
                        <Form.Control size='lg' required className='text-input' value={credentials.email} onChange={(event)=>{setCredentials({...credentials, email:event.target.value})}} />
                    </Form.Group>
                    <Form.Group  className="mb-3 first-half">
                        <Form.Label className='text-label'>H.R. Name</Form.Label>
                        <Form.Control name='hrName'  size='lg' className='text-input' value={credentials.hrName} onChange={(event)=>{setCredentials({...credentials, hrName:event.target.value})}} />
                    </Form.Group>
                    <Form.Group  className="mb-3 second-half">
                        <Form.Label className='text-label'>H.R. Phone Number</Form.Label>
                        <Form.Control name='hrContactNumber' type='tel' size='lg' className='text-input' value={credentials.hrContactNumber} maxLength={10} onChange={(event)=>{setCredentials({...credentials, hrContactNumber:event.target.value.replace(/[^0-9]/g, '')})}} />
                    </Form.Group>

                    <Form.Group className="mb-3 end-to-end " controlId="exampleForm.ControlTextarea1">
                        <Form.Label className='text-label'>Company Address</Form.Label>
                        <Form.Control name='companyAddress' required value={credentials.companyAddress} className='text-input' style={{minHeight:'100px'}} as="textarea" rows={3} onChange={(event)=>{setCredentials({...credentials, companyAddress:event.target.value})}} />
                    </Form.Group>
                    
                </Form>

                <div className='button-container'>
                    <Button variant="primary" disabled={loading} className='button' type="submit" onClick={handleSubmit}>
                        Save
                    </Button>

                    {
                        credentials.author === "ADMIN"
                        &&
                        <Button variant="secondary" disabled={loading} className='delete-button' type="submit" onClick={()=>{setDeleteModal(true)}}>
                            Delete Company
                        </Button>
                    }
                </div>

            </div>
            <ToastContainer
                position="top-right"
                autoClose={2000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                />
            <ToastContainer />

            <Modal show={deleteModal} onHide={()=>{setDeleteModal(false)}}>
                <Modal.Header closeButton>
                    <Modal.Title>Confirm!</Modal.Title>
                </Modal.Header>
                
                <Modal.Body>
                    <p>Are you sure you want to permanently delete this company?</p>
                </Modal.Body>
                
                <Modal.Footer>
                    <Button variant="secondary" onClick={()=>{setDeleteModal(false)}}>Cancel</Button>
                    <Button variant="primary" onClick={()=>{handleDelete(); setDeleteModal(false)}}>Delete</Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}

export default CompanyDetail;