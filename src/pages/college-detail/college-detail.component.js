import React, { useState } from 'react';
import './college-detail.style.scss'
import logo from '../../assets/logo.png'
import { Button, Form, Image, Spinner, Modal } from 'react-bootstrap';
import { useLocation, useNavigate } from 'react-router-dom';
import Header from '../../components/header/header.component';
import { get, ref, set, push, remove } from 'firebase/database';
import { firebaseDatabase, firebaseStorage } from '../../backend/firebase-handler';
import { ToastContainer, toast } from "react-toastify";
import { ref as firebaseStorageRef, uploadBytesResumable, getDownloadURL } from 'firebase/storage';
import { IoMdArrowRoundBack } from 'react-icons/io'

const CollegeDetailPage = () => {

    const location = useLocation()
    const [credentials, setCredentials] = useState(location.state.item?location.state.item:{companyLogo:"https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COLLEGE_LOGO%2Fcollege.png?alt=media&token=4b433e19-8007-4c8c-ad03-c6bd70d1f113"});
    const [title, setTitle] = useState(location.state.type==="VIEW"?"Company Detail":"Add Company")
    const [uploadingImage, setUploadingImage] = useState(false);  
    const [uploadPercentage, setUploadPercentage] = useState(0);
    const [deleteModal, setDeleteModal] = useState(false)
    const [loading, setLoading] = useState(false)
    const navigate = useNavigate()

    const handleSubmit = async (event) => {
        event.preventDefault();
        setLoading(true)
        if (location.state.type === "VIEW") {
            if (credentials.placementOfficerPhoneNumber.length !== 10) {
                toast.warn("Please enter a valid PO Phone Number")
                setLoading(false)
                return
            }
            const profileRef = ref(firebaseDatabase, `COLLEGE_ARCHIVE/`+credentials.uid);
            await set(profileRef, {...credentials});
            toast.success(`Saved Changes`);
            setLoading(false)
        } else {
            if (credentials.placementOfficerPhoneNumber.length !== 10) {
                toast.warn("Please enter a valid PO Phone Number")
                setLoading(false)
                return
            }
            var date = new Date().getDate(); 
            var month = new Date().getMonth() + 1; 
            var year = new Date().getFullYear(); 
            var hours = new Date().getHours(); 
            var min = new Date().getMinutes();
            var sec = new Date().getSeconds(); 
            credentials.date = date + "-" + month + "-" + year + " " + hours + ":" + min + ":" + sec
            credentials.author = "ADMIN"
            if (credentials.uid === "" || credentials.uid === undefined) {
                credentials.uid = push(ref(firebaseDatabase, "COLLEGE_ARCHIVE")).key
            }
            await set(ref(firebaseDatabase, "COLLEGE_ARCHIVE/"+credentials.uid), credentials);
            toast.success("College added");
            setLoading(false)

            const timer = setTimeout(() => {
                navigate(-1)
            }, 1500);
            return () => clearTimeout(timer);   
        }
    }

    const handleLogoChange = () => {
        const inputElement = document.createElement('input');
        inputElement.setAttribute('type',"file");
        inputElement.setAttribute('accept','image/*');
        inputElement.onchange = (event) => {
            const files = event.target.files;
            let id = ""
            if (location.state.type==="VIEW") {
                id = credentials.uid
            } else {
                id = push(ref(firebaseDatabase, "COLLEGE_ARCHIVE")).key
                credentials.uid = id
            }
            const logoRef = firebaseStorageRef(firebaseStorage, "COLLEGE_LOGO/"+id+"/logo");            
            const uploadTask = uploadBytesResumable(logoRef,files[0], {contentType:'image/png'}); 
            uploadTask.on('state_changed', (snapshot) => {
                setUploadingImage(true);
                const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                setUploadPercentage(progress.toFixed(2));
            }, () => {
                toast.error('Something went wrong');
                setUploadingImage(false);
            }, async () => {
                const url = await getDownloadURL(logoRef);
                setCredentials({...credentials, companyLogo:url});
                setUploadingImage(false);
            })

        }
        inputElement.click();

    }

    const handleDelete = async () => {
        setLoading(true)

        await set(ref(firebaseDatabase, "DELETED_COLLEGES/"+credentials.uid), credentials)
        await remove(ref(firebaseDatabase, "COLLEGE_ARCHIVE/"+credentials.uid))

        toast.success("College Deleted")
        const timer = setTimeout(() => {
            navigate(-1)
        }, 1500);
        return () => clearTimeout(timer);
    }

    return(
        <div className='company-profile-container'>
             <div className="back-logo-container">
                <IoMdArrowRoundBack  size={20}  style={{cursor:"pointer"}} onClick={()=>{navigate(-1)}} />
                <img src={logo} alt="OccuHire" className="logo" />
            </div>
            <div className='company-profile-content'>
                <h2 className='section-title' >College detail</h2>
                
                {
                    uploadingImage
                    ?
                    <div className='profile-logo' >
                        <Spinner animation="border" size={20} /> 
                        <span>{uploadPercentage}% Uploaded</span> 
                    </div>
                    :
                    <Image src={credentials.companyLogo?credentials.companyLogo:"https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COLLEGE_LOGO%2Fcollege.png?alt=media&token=4b433e19-8007-4c8c-ad03-c6bd70d1f113"} onClick={handleLogoChange} className='profile-logo' roundedCircle />
                }
                <Form className='main-form' >
                    <Form.Group  className="mb-3 first-half">
                        <Form.Label className='text-label '>College Name</Form.Label>
                        <Form.Control required size='lg' className='text-input' disabled={location.state.type==="VIEW"} value={credentials.collegeName} onChange={(event)=>{setCredentials({...credentials, collegeName:event.target.value})}} />
                    </Form.Group>

                    <Form.Group  className="mb-3 second-half">
                        <Form.Label className='text-label'>Email Id</Form.Label>
                        <Form.Control required  size='lg' className='text-input' disabled={location.state.type==="VIEW"} value={credentials.emailId} onChange={(event)=>{setCredentials({...credentials, emailId:event.target.value})}}  />
                    </Form.Group>
                    <Form.Group  className="mb-3 first-half">
                        <Form.Label className='text-label'>Placement Officer Name</Form.Label>
                        <Form.Control required name='placementOfficerName'  size='lg' className='text-input' value={credentials.placementOfficerName} onChange={(event)=>{setCredentials({...credentials, placementOfficerName:event.target.value})}}  />
                    </Form.Group>
                    <Form.Group  className="mb-3 second-half">
                        <Form.Label className='text-label'>Placement Officer Phone Number</Form.Label>
                        <Form.Control required name='placementOfficerPhoneNumber' type='tel' size='lg' className='text-input' value={credentials.placementOfficerPhoneNumber} maxLength={10} onChange={(event)=>{setCredentials({...credentials, placementOfficerPhoneNumber:event.target.value.replace(/[^0-9]/g, '')})}}  />
                    </Form.Group>

                    <Form.Group className="mb-3 end-to-end " controlId="exampleForm.ControlTextarea1">
                        <Form.Label className='text-label'>College Address</Form.Label>
                        <Form.Control required name='collegeAddress' value={credentials.collegeAddress} className='text-input' style={{minHeight:'100px'}} as="textarea" rows={3} onChange={(event)=>{setCredentials({...credentials, collegeAddress:event.target.value})}}  />
                    </Form.Group>
                </Form>

                <div className='button-container'>
                    <Button variant="primary" disabled={loading} className='button' type="submit" onClick={handleSubmit}>
                        Save
                    </Button>

                    {
                        credentials.author === "ADMIN"
                        &&
                        <Button variant="secondary" disabled={loading} className='delete-button' type="submit" onClick={()=>{setDeleteModal(true)}}>
                            Delete College
                        </Button>
                    }
                </div>
            </div>

            <ToastContainer
                position="top-right"
                autoClose={2000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                />
            <ToastContainer />

            <Modal show={deleteModal} onHide={()=>{setDeleteModal(false)}}>
                <Modal.Header closeButton>
                    <Modal.Title>Confirm!</Modal.Title>
                </Modal.Header>
                
                <Modal.Body>
                    <p>Are you sure you want to permanently delete this college?</p>
                </Modal.Body>
                
                <Modal.Footer>
                    <Button variant="secondary" onClick={()=>{setDeleteModal(false)}}>Cancel</Button>
                    <Button variant="primary" onClick={()=>{handleDelete(); setDeleteModal(false)}}>Delete</Button>
                </Modal.Footer>
            </Modal>
        </div>
    )
}

export default CollegeDetailPage;