import React, { useEffect, useState } from "react";
import { firebaseAuth } from "../../backend/firebase-handler";
import './home.style.scss'
import { Image } from 'react-bootstrap';
import logo from '../../assets/logo.png';
import { Helmet } from 'react-helmet';
import SidebarNavigation from '../../components/sidebar-navigation/sidebar-navigation.component';
import { useNavigate, useLocation } from 'react-router-dom';
import Dashboard from "../../components/dashboard/dashboard.component";
import CurrentJobs from "../../components/current-jobs/current-jobs.component";
import Companies from "../../components/companies/companies.component";
import Assessment from "../../components/assessment/assessment.component";
import Colleges from "../../components/colleges/colleges.component";
import CollapsableNavigation from "../../components/collapsable-navigation/collapsable-navigation.component";
import Students from "../../components/students/students.compoennt";
import Reports from "../../components/reports/reports.component";
import Banners from "../../components/banners/banners.component";

const HomePage = ({setCurrentState}) => {

    const firebaseUser = firebaseAuth.currentUser;
    const navigate = useNavigate();
    const location = useLocation()
    const [selectedTab, setSelectedTab] = useState("Dashboard")

    useEffect(() => {
        // firebaseAuth.signOut()

        if (!firebaseUser){
            setCurrentState("LOGIN")
        }

        if (location.search){
            const params = new URLSearchParams(location.search);
            setSelectedTab(params.get('tab'));
        }
    },[])

    const handleChange = (option) => {
        setSelectedTab(option)
    }
 
    return(
        <div className='home-container'>
            <Helmet>
                {
                    firebaseUser
                    ?
                    <title>OccuHire </title>
                    :
                    <title>OccuHire</title>
                }
                
            </Helmet>
            
            <div className='sidebar'>
                <Image className='logo' src={logo} alt='OccuHire' />
                <SidebarNavigation onChange={handleChange} />
            </div>
            
            <div className='content'>

                <div className='collapsable-sidebar-wrapper'>
                    <CollapsableNavigation  onChange={handleChange} />
                </div>
                {
                    selectedTab === "Dashboard"
                    &&
                    <Dashboard />
                }
                {
                    selectedTab === "ActiveNewJobs"
                    &&
                    <CurrentJobs />
                }
                {
                    selectedTab === "PostJob"
                    &&
                    <Companies type={"POST"} />
                }
                {
                    selectedTab === "ManageCompanies"
                    &&
                    <Companies type={"VIEW"} />
                }
                {
                    selectedTab === "StudentsData"
                    &&
                    <Students />
                }
                {
                    selectedTab === "Colleges"
                    &&
                    <Colleges />
                }
                {
                    selectedTab === "PreQualifyingExams"
                    &&
                    <Assessment type={"Pre Qualifying Exams"} />
                }
                {
                    selectedTab === "Assessments"
                    &&
                    <Assessment type={"Skill Assessments"} />
                }
                {
                    selectedTab === "Reports"
                    &&
                    <Reports />
                }
                {
                    selectedTab === "Banners"
                    &&
                    <Banners />
                }
            </div>

        </div>
    )
}

export default HomePage