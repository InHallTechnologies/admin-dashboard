import React, { useState } from 'react'
import { Button } from 'react-bootstrap'
import Header from '../header/header.component'
import LandingBanner from '../landing-banner/landing-banner.component'
import Logos from '../logos/logos.component'
import StudentBanner from '../student-banner/student-banner.component'
import './banners.style.scss'

const Banners = () => {

    const [selectedTab, setSelectedTab] = useState("STUDENT") 

    return(
        <div className='banners-container'>
            <Header />

            <div className="tab-container">
                <Button variant="primary" className={selectedTab==="STUDENT"?"selected":"unselected"} onClick={()=>{setSelectedTab("STUDENT")}}>Student Portal</Button>
                <Button variant="primary" className={selectedTab==="LANDING"?"selected":"unselected"} onClick={()=>{setSelectedTab("LANDING")}}>Placement Stats/Testimonials</Button>
                <Button variant="primary" className={selectedTab==="RECRUITER"?"selected":"unselected"} onClick={()=>{setSelectedTab("RECRUITER")}}>Top Recruiters</Button>
                <Button variant="primary" className={selectedTab==="UNIVERSITY"?"selected":"unselected"} onClick={()=>{setSelectedTab("UNIVERSITY")}}>University Partners</Button>
            </div>

            {
                selectedTab === "STUDENT"
                &&
                <StudentBanner />
            }
            {
                selectedTab === "LANDING"
                &&
                <LandingBanner />
            }
            {
                selectedTab === "RECRUITER"
                &&
                <Logos type="RECRUITER" />
            }
            {
                selectedTab === "UNIVERSITY"
                &&
                <Logos type="UNIVERSITY" />
            }
        </div>
    )
}

export default Banners