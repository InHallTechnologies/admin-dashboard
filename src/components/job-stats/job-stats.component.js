import React, { useEffect, useState } from "react";
import { Form, Spinner, Table } from "react-bootstrap";
import "./job-stats.style.scss"
import DatePicker from 'react-datepicker';
import { child, get, ref } from "firebase/database";
import { firebaseDatabase } from "../../backend/firebase-handler";

const JobStats = ({ type }) => {

    const [startDate, setStartDate] = useState(new Date());
    const [jobList, setJobList] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(()=>{
        var temp = []

        if (type === "ADMIN") {
            get(child(ref(firebaseDatabase), "APPROVED_VACANCY")).then((snap)=>{
                if(snap.exists()) {
                    for (const key in snap.val()) {
                        if (snap.child(key).child("postingDate").val().split(" ")[0].split("-")[2]===new Date().getFullYear().toString() && snap.child(key).child("postingDate").val().split(" ")[0].split("-")[1]===(new Date().getMonth()+1).toString()) {
                            temp.push(snap.child(key).val())
                        }
                    }
                    setJobList(temp.reverse())
                    setLoading(false)
                } else {
                    setLoading(false)
                }
            })
        } else {
            get(child(ref(firebaseDatabase), "COLLEGE_VACANCY")).then((snap) => {
                if (snap.exists()) {
                    for (const uid in snap.val()) {
                        for (const key in snap.child(uid).val()) {
                            if (snap.child(uid).child(key).child("postingDate").val().split(" ")[0].split("-")[2]===new Date().getFullYear().toString() && snap.child(uid).child(key).child("postingDate").val().split(" ")[0].split("-")[1]===(new Date().getMonth()+1).toString()) {
                                temp.push(snap.child(uid).child(key).val())
                            }
                        }
                    }
                    setJobList(temp.reverse())
                    setLoading(false)
                } else {
                    setLoading(false)
                }
            })
        }
    }, [])

    const handleSelect = async (date) => {

        var temp = []

        if (type === "ADMIN") {
            await get(child(ref(firebaseDatabase), "APPROVED_VACANCY")).then((snap)=>{
                if(snap.exists()) {
                    for (const key in snap.val()) {
                        if (snap.child(key).child("postingDate").val().split(" ")[0].split("-")[2]===date.getFullYear().toString() && snap.child(key).child("postingDate").val().split(" ")[0].split("-")[1]===(date.getMonth()+1).toString()) {
                            temp.push(snap.child(key).val())
                        }
                    }
                    setJobList(temp.reverse())
                    setLoading(false)
                } else {
                    setJobList(temp.reverse())
                    setLoading(false)
                }
            })
        } else {
            await get(child(ref(firebaseDatabase), "COLLEGE_VACANCY")).then((snap) => {
                if (snap.exists()) {
                    for (const uid in snap.val()) {
                        for (const key in snap.child(uid).val()) {
                            if (snap.child(uid).child(key).child("postingDate").val().split(" ")[0].split("-")[2] === date.getFullYear().toString() && snap.child(uid).child(key).child("postingDate").val().split(" ")[0].split("-")[1]===(date.getMonth()+1).toString()) {
                                temp.push(snap.child(uid).child(key).val())
                            }
                        }
                    }
                    setJobList(temp.reverse())
                    setLoading(false)
                } else {
                    setJobList(temp.reverse())
                    setLoading(false)
                }
            })
        }
    }

    return(
        <div className="job-stats-container">
            <Form.Group className="year-picker" controlId="exampleForm.ControlInput1">
                <Form.Label className={'text-label'}>Select Month and Year</Form.Label>
                <DatePicker
                    selected={startDate}
                    onChange={(date) => {setStartDate(date); handleSelect(date);}}
                    showMonthYearPicker
                    dateFormat="MM yyyy"
                    className='picker-input'
                />
            </Form.Group>

            <p className="job-count">Total jobs posted: {jobList.length}</p>

            {
                loading
                ?
                <Spinner animation="border" variant="primary" style={{marginTop:50, alignSelf:"center"}} />
                :
                jobList.length === 0
                ?
                <p style={{marginTop:50, marginBottom:20, alignSelf:"center", fontWeight:600, fontSize:16, color:"#ACACAC"}}>No logs here!</p>
                :
                <Table hover className="table" responsive>
                    <thead>
                        <tr >
                                <th>Company Name</th>
                                <th>Role</th>
                                <th>Date</th>
                                <th>Package</th>
                                <th>Posted By</th>
                        </tr>
                    </thead>
                    <tbody className="table-body">
                        {
                            jobList.map((item, index) => {return(
                                <tr>
                                    <td>
                                        <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                                            <img src={item.companyLogo} alt={item.companyName} style={{width:50, height:50, borderRadius:50, marginRight:10}} />
                                            <p>{item.companyName}</p>
                                        </div>
                                    </td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.role}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.postingDate.split(" ")[0]}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.startPackage}L - {item.endPackage}L</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{type==="ADMIN"?item.author==="ADMIN"?"Admin":"Company":item.collegeName}</div></td>
                                </tr>
                            )})
                        }
                    </tbody>
                </Table>
            }
        </div>
    )
}

export default JobStats