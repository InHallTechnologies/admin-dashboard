import React, { useEffect, useState } from "react";
import Header from "../header/header.component";
import './companies.style.scss'
import { BiSearchAlt } from 'react-icons/bi'
import { firebaseDatabase } from "../../backend/firebase-handler";
import { get, ref, child, query, limitToLast } from 'firebase/database'
import { Image, Button, Table, Row } from 'react-bootstrap';
import { useNavigate } from "react-router-dom";

const Companies = ({type}) => {

    const [companies, setCompanies] = useState([])
    const [originalList, setOriginalList] = useState([])
    const [title, setTitle] = useState(type==="VIEW"?"On-boarded Companies":"Select a company")
    const [searchWord, setSearchWord] = useState("")
    const [buttonTag, setButtonTag] = useState(type==="VIEW"?"Detail":"Select")
    const navigation = useNavigate()

    useEffect(() => {

        var tempCompanies = []

        const refCollege = ref(firebaseDatabase, "COMPANY_ARCHIVE")
        get(child(ref(firebaseDatabase), "COMPANY_ARCHIVE")).then((snapShot) => {
            if (snapShot.exists()) {
                for (const key in snapShot.val()) {
                    var item = snapShot.child(key).val()
                    item.companyUID = key
                    tempCompanies.push(item)
                }
                setCompanies(tempCompanies.reverse())
                setOriginalList(tempCompanies)
            }
        })
    }, [])

    const handleSearch = () => {
        var temp = []

        if (searchWord==="") {
            setCompanies(originalList)
        } else {
            for (const index in originalList) {
                console.log(originalList[index])
                if (originalList[index].companyName.toLowerCase().includes(searchWord.toLowerCase())) {
                    temp.push(originalList[index])
                }
            }
            setCompanies(temp)
        }
    }

    return(
        <div className="companies-container">
            <Header />

            <div className="search-container">
                <BiSearchAlt color="#ACACAC" size={18} />
                <input className="search-bar" placeholder="Company Name" type="text" value={searchWord} onKeyUp={(event)=>{if(event.key === 'Enter' || event.keyCode === 13){handleSearch()}}} onChange={(event)=>{setSearchWord(event.target.value)}} />
            </div>

            <div className="drives-container">
                <div className="title-button-container">
                    <p className="section-title">{title}</p>
                    <Button variant="primary" className="button" style={type==="VIEW"?null:{display:"none"}} onClick={()=>{navigation("/company-detail", {state:{type:"ADD"}})}} >Add company</Button>
                </div>
               

                <Table hover className="table" responsive>
                    <thead>
                        <tr >
                            <th>Company</th>
                            <th>HR Name</th>
                            <th>Phone Number</th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody className="table-body">
                        {
                            companies.map((item, index) => {return(
                                <tr>
                                    <td>
                                        <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                                            <img src={item.companyLogo?item.companyLogo:"https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fbuildings.png?alt=media&token=41b80aa2-3348-4616-8b04-5f155d9e0b55"} alt={item.companyName} style={{width:50, height:50, borderRadius:50, marginRight:10}} />
                                            <p>{item.companyName}</p>
                                        </div>
                                    </td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.hrName}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.hrContactNumber}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}><Button className="upgrade-button" variant="primary" onClick={()=>{if(type==="VIEW"){navigation("/company-detail", {state:{item:item, type:"VIEW"}})} else{navigation("/job-detail", {state:{type:"ADD", company:item}})}}}>{buttonTag}</Button></div></td>
                                </tr>
                            )})
                        }
                    </tbody>
                </Table>
            </div>
        </div>
    )
}

export default Companies