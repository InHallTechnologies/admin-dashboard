import { child, get, ref, set } from "firebase/database";
import React, { useEffect, useState } from "react";
import { firebaseDatabase, firebaseStorage } from "../../backend/firebase-handler";
import "./logos.style.scss"
import { getDownloadURL, uploadBytesResumable, ref as firebaseStorageRef } from 'firebase/storage'
import { BsImage } from 'react-icons/bs'
import { MdOutlineDeleteOutline } from 'react-icons/md'

const Logos = ({type}) => {

    const [logoList, setLogoList] = useState([])
    const [loading, setLoading] = useState(true)
    const [uploadingImage, setUploadingImage] = useState(false)

    useEffect(() => {
        get(child(ref(firebaseDatabase), "LOGO_ARCHIVE/"+type)).then((snap)=>{
            if (snap.exists()) {
                setLogoList(snap.val())
                setLoading(false)
            } else {
                setLoading(false)
            }
        })
    }, [])

    const handleDelete = async (url) => {
        const newFilter = logoList.filter((currentItem) => currentItem !== url)
        set(ref(firebaseDatabase, "LOGO_ARCHIVE/"+type), newFilter)
        setLogoList(newFilter)
    }

    const pickImage = (index) => {
        const inputElement = document.createElement('input');
        inputElement.setAttribute('type',"file");
        inputElement.setAttribute('accept','image/*');
        inputElement.onchange = (event) => {
            const files = event.target.files;
            const FILE_NAME = Date.now() + "--"+type;
            const logoRef = firebaseStorageRef(firebaseStorage, "PARTNER_LOGOS/"+type+"/"+FILE_NAME);            
            const uploadTask = uploadBytesResumable(logoRef,files[0], {contentType:'image/png'}); 
            uploadTask.on('state_changed', (snapshot) => {
                setUploadingImage(true);
                const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                // setUploadPercentage(progress.toFixed(2));
            }, () => {
                // toast.error('Something went wrong');
                setUploadingImage(false);
            }, async () => {
                const url = await getDownloadURL(logoRef);
                logoList[index] = url
                setLogoList(logoList)
                set(ref(firebaseDatabase, "LOGO_ARCHIVE/"+type), logoList)
                setUploadingImage(false);
            })

        }
        inputElement.click();
    } 

    return(
        <div className="partner-logo-container">
            <div className='add-title-container'>
                <p className="title">Add banner</p>
                <div onClick={()=>{if(!uploadingImage){pickImage(logoList.length)}}} className="add-image-container">
                    <BsImage color='#ccc' size={30} />
                    <p className="add-image-tag">{uploadingImage?"Uploading a logo...":"Click to add image"}</p>
                </div>
            </div>
            
            {
                logoList.map((item, index)=>{return(
                    <div className="single-banner-container">
                        {/* <p className="title">Banner {index+1}</p> */}
                        <div className="add-banner-container">
                            {
                                <div className="banner-delete-container">
                                    <img src={item} alt="first banner" title="Click to change" onClick={()=>{pickImage(index)}} className="banner-image" />
                                    <div onClick={()=>{handleDelete(item)}} className="delete-container" >
                                        <MdOutlineDeleteOutline color="CF4539" size={20}  />
                                        {/* <p className="delete">Delete Banner</p> */}
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                )})
            }
        </div>
    )
}

export default Logos