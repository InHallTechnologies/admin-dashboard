import React, { useEffect, useState } from "react";
import Header from "../header/header.component";
import './current-jobs.style.scss'
import { BiSearchAlt } from 'react-icons/bi'
import { Button, Table } from "react-bootstrap";
import { firebaseDatabase } from "../../backend/firebase-handler";
import { get, child, ref } from 'firebase/database'
import { useNavigate } from "react-router-dom";

const CurrentJobs = () => {

    const [newList, setNewList] = useState([])
    const [activeList, setActiveList] = useState([])
    const [drivesList, setDrivesLIst] = useState([])
    const [selectedTab, setSelectedTab] = useState("ACTIVE")
    const [searchWord, setSearchWord] = useState("")
    const navigation = useNavigate()

    useEffect(() => {
        let tempNewDrives = []
        let tempActiveDrives = []

        get(child(ref(firebaseDatabase), "BUFFER_VACANCY")).then((snapShot) => {
            if (snapShot.exists()) {
                for (const key in snapShot.val()) {
                    tempNewDrives.push(snapShot.child(key).val())
                }
                setNewList(tempNewDrives.reverse())
            }
        })

        get(child(ref(firebaseDatabase), "APPROVED_VACANCY")).then((snapShot) => {
            if (snapShot.exists()) {
                for (const key in snapShot.val()) {
                    tempActiveDrives.push(snapShot.child(key).val())
                }
                setActiveList(tempActiveDrives.reverse())
                setDrivesLIst(tempActiveDrives)
            }
        })
    }, [])

    const handleSearch = () => {
        let temp = []
        if (selectedTab === "ACTIVE") {
            if (searchWord==="") {
                setDrivesLIst(activeList)
            } else {
                for (const index in activeList) {
                    if (activeList[index].companyName.toLowerCase().includes(searchWord.toLowerCase()) || activeList[index].location.toLowerCase().includes(searchWord.toLowerCase()) || activeList[index].role.toLowerCase().includes(searchWord.toLowerCase())) {
                        temp.push(activeList[index])
                    }
                }
                setDrivesLIst(temp)
            }
        } else {
            if (searchWord==="") {
                setDrivesLIst(newList)
            } else {
                for (const index in drivesList) {
                    if (newList[index].companyName.toLowerCase().includes(searchWord.toLowerCase()) || newList[index].location.toLowerCase().includes(searchWord.toLowerCase()) || newList[index].role.toLowerCase().includes(searchWord.toLowerCase())) {
                        temp.push(newList[index])
                    }
                }
                setDrivesLIst(temp)
            }
        }
    }

    return(
        <div className="current-jobs-container">
            <Header />
            
            <div className="search-container">
                <BiSearchAlt color="#ACACAC" size={18} />
                <input className="search-bar" placeholder="Company Name, Role or Location" type="text" value={searchWord} onKeyUp={(event)=>{if(event.key === 'Enter' || event.keyCode === 13){handleSearch()}}} onChange={(event)=>{setSearchWord(event.target.value)}} />
            </div>

            <div className="tab-container">
                <Button variant="primary" className={selectedTab==="ACTIVE"?"selected":"unselected"} onClick={()=>{setSelectedTab("ACTIVE"); setDrivesLIst(activeList); setSearchWord("");}}>Active Jobs</Button>
                <Button variant="primary" className={selectedTab==="NEW"?"selected":"unselected"} onClick={()=>{setSelectedTab("NEW"); setDrivesLIst(newList); setSearchWord("");}}>New Jobs</Button>
            </div>

            <div className="drives-list-container">
                <p className="section-title">Manage Active/New Jobs</p>

                <Table hover className="table" responsive>
                    <thead>
                        <tr >
                                <th>Company</th>
                                <th>Role</th>
                                <th>Package</th>
                                <th>Location</th>
                                <th> </th>
                        </tr>
                    </thead>
                    <tbody className="table-body">
                        {
                            drivesList.map((item, index) => {return(
                                <tr>
                                    <td>
                                        <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                                            <img src={item.companyLogo?item.companyLogo:"https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fbuildings.png?alt=media&token=41b80aa2-3348-4616-8b04-5f155d9e0b55"} alt={item.companyName} style={{width:50, height:50, borderRadius:50, marginRight:10}} />
                                            <p>{item.companyName}</p>
                                        </div>
                                    </td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.role}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.startPackage}L - {item.endPackage}L</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.location}</div></td>
                                    {
                                        selectedTab==="ACTIVE"
                                        ?
                                        <td><div style={{display:"flex", flexDirection:"row", alignItems:"center", height:50}}>
                                            <Button className="upgrade-button" variant="primary" onClick={()=>{navigation("/job-detail", {state:{item:item, type:"VIEW"}})}}>View/Edit</Button>
                                            <Button className="upgrade-button" style={{backgroundColor:"#52E1E2"}} variant="primary" onClick={()=>{navigation("/applications", {state:{id:item.jobId, company:item.companyUID, role:item.role, companyName:item.companyName}});}}>Application</Button>
                                        </div></td>
                                        :
                                        <td><div style={{display:"flex", alignItems:"center", height:50}}><Button className="upgrade-button" variant="primary" onClick={()=>{navigation("/job-detail", {state:{item:item, type:"ACT"}})}}>Approve/Reject</Button></div></td>
                                    }
                                </tr>
                            )})
                        }
                    </tbody>
                </Table>
            </div>

        </div>
    )
}

export default CurrentJobs