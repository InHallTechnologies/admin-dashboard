import React, { useState } from "react";
import { Button } from "react-bootstrap";
import AssessmentResults from "../assessment-results/assessment-reslts.component";
import Header from "../header/header.component";
import JobStats from "../job-stats/job-stats.component";
import Logs from "../logs/logs.compoennt";
import "./reports.style.scss"

const Reports = () => {

    const [selectedTab, setSelectedTab] = useState("RESULTS")

    return(
        <div className="reports-container">
            <Header />

            <div className="tab-container">
                <Button variant="primary" className={selectedTab==="RESULTS"?"selected":"unselected"} onClick={()=>{setSelectedTab("RESULTS")}}>Assessment Results</Button>
                <Button variant="primary" className={selectedTab==="COLLEGE_JOBS"?"selected":"unselected"} onClick={()=>{setSelectedTab("COLLEGE_JOBS")}}>Jobs by College</Button>
                <Button variant="primary" className={selectedTab==="ADMIN_JOBS"?"selected":"unselected"} onClick={()=>{setSelectedTab("ADMIN_JOBS")}}>Jobs by Admin</Button>
                <Button variant="primary" className={selectedTab==="COLLEGE_LOGS"?"selected":"unselected"} onClick={()=>{setSelectedTab("COLLEGE_LOGS")}}>College Logs</Button>
                <Button variant="primary" className={selectedTab==="RECRUITER_LOGS"?"selected":"unselected"} onClick={()=>{setSelectedTab("RECRUITER_LOGS")}}>Recruiter Logs</Button>
            </div>

            {
                selectedTab === "RESULTS"
                &&
                <AssessmentResults />
            }
            {
                selectedTab === "COLLEGE_JOBS"
                &&
                <JobStats type="COLLEGE" />
            }
            {
                selectedTab === "ADMIN_JOBS"
                &&
                <JobStats type="ADMIN" />
            }
            {
                selectedTab === "COLLEGE_LOGS"
                &&
                <Logs type="COLLEGE" />
            }
            {
                selectedTab === "RECRUITER_LOGS"
                &&
                <Logs type="RECRUITER" />
            }
        </div>
    )
}

export default Reports