import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "./students.style.scss"
import { get, ref, child } from 'firebase/database'
import { firebaseDatabase } from '../../backend/firebase-handler';
import Header from "../header/header.component";
import { Button, Form, Spinner, Table } from "react-bootstrap";
import DatePicker from 'react-datepicker';
import departmentList from "../../entities/departmentList";
import SearchBar from "../search-bar/SearchBar.component";

const Students = () => {

    const [loading, setLoading] = useState(true);
    const [studentList, setStudentList] = useState([])
    const [startDate, setStartDate] = useState(new Date());
    const [selectedDepartment, setSelectedDepartment] = useState("");
    const [selectedCollege, setSelectedCollege] = useState("")
    const [collegeList, setCollegeList] = useState([])
    const navigate = useNavigate()
    const [searchQuery, setSearchQuery] = useState("");
    const [applyingFilter, setApplyingFilter] = useState(false)

    useEffect(() => {
        var temp = []

        get(child(ref(firebaseDatabase), "COLLEGE_ARCHIVE")).then((snap)=>{
            if(snap.exists()) {
                for (const key in snap.val()) {
                    temp.push(snap.child(key).val())
                }
                setCollegeList(temp)
                setLoading(false)
            } else {
                setLoading(false)
            }
        })
    }, [])

    const handleFilter = async () => {
        setLoading(true)
        var temp = []
        await get(child(ref(firebaseDatabase), "COLLEGE_WISE_STUDENTS/"+selectedCollege+"/"+selectedDepartment)).then((snapShot)=>{
            if (snapShot.exists()) {
                for  (const key in snapShot.val()) {
                    if (snapShot.child(key).child("yearOfStudy").val() === startDate.getFullYear().toString()) {
                        temp.push(snapShot.child(key).val())
                    }
                }
                setStudentList(temp)
                setLoading(false)
            } else {
                setStudentList(temp)
                setLoading(false)
            }
        })
    }

    const searchHandler = async () => {
        setApplyingFilter(true)
        let tempStudents = []
        await get(child(ref(firebaseDatabase), "COLLEGE_WISE_STUDENTS/")).then( async (snap)=>{
            if (snap.exists()) {
                for (const college in snap.val()) {
                    await get(child(ref(firebaseDatabase), "COLLEGE_WISE_STUDENTS/"+college)).then( async (snap2) => {
                        for (const department in snap2.val()) {
                            await get(child(ref(firebaseDatabase), "COLLEGE_WISE_STUDENTS/"+college+"/"+department)).then((snap3)=>{
                                console.log(college, department)
                                for (const key in snap3.val()) {
                                    console.log(snap3.child(key).child("usn").val())
                                    if (snap3.child(key).child("usn").val().toLowerCase() === searchQuery.toLowerCase()) {
                                        tempStudents.push(snap3.child(key).val())
                                    }
                                }
                            })
                        }
                    })
                }
                setStudentList(tempStudents)
                setApplyingFilter(false)
                console.log(tempStudents[0], " aaa")
            } else {
                setApplyingFilter(false)
            }
        })
    }

    return(
        <div className="student-data-container">
            <Header />

            <SearchBar placeholder={'Search by USN'} onSearchEntered={searchHandler} searchWord={searchQuery} setSearchWord={setSearchQuery} />

            <div className={'content'} >
                <div className="table-content">
                    <h2 className="section-title">
                        Student Data
                    </h2>

                    <div className="filter-container">
                        <Form.Group className="mb-3 department-picker" controlId="exampleForm.ControlInput1">
                            <Form.Label className={'text-label'}>College</Form.Label>
                            <Form.Select value={selectedCollege} onChange={event => setSelectedCollege(event.target.value)} className={'text-input picker-input'} aria-label="Default select example">
                                <option>Select College</option>
                                {
                                    collegeList.map(item => <option key={item.uid} value={item.uid} >{item.collegeName}</option>)
                                }
                            </Form.Select>
                        </Form.Group>
                        <Form.Group className="mb-3 department-picker" controlId="exampleForm.ControlInput1">
                            <Form.Label className={'text-label'}>Department</Form.Label>
                            <Form.Select value={selectedDepartment} onChange={event => setSelectedDepartment(event.target.value)} className={'text-input picker-input'} aria-label="Default select example">
                                <option>Select department</option>
                                {
                                    departmentList.map(item => <option key={item} >{item}</option>)
                                }
                            </Form.Select>
                        </Form.Group>
                        <Form.Group className="mb-3 year-picker" controlId="exampleForm.ControlInput1">
                            <Form.Label className={'text-label'}>Year</Form.Label>
                            <DatePicker
                                selected={startDate}
                                onChange={(date) => setStartDate(date)}
                                showYearPicker
                                dateFormat="yyyy"
                                className='picker-input'
                            />
                        </Form.Group>

                        
                        <Button disabled={applyingFilter} className="search-button" variant="primary" onClick={handleFilter}>Apply filter</Button>
                    </div>

                    {
                        loading
                        ?
                        <Spinner animation="border" variant="primary" style={{marginTop:50, alignSelf:"center", marginLeft:"auto"}} />
                        :
                        studentList.length === 0
                        ?
                        <p style={{marginTop:50, alignSelf:"center", fontWeight:600, fontSize:16, color:"#ACACAC", marginLeft:20}}>No students here!</p>
                        :
                        <Table className="table" hover >
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>USN</th>
                                    <th>Semester</th>
                                    <th>No. of offers</th>
                                </tr>
                            </thead>
                            <tbody className="table-body">
                                {
                                    studentList.map((item, index) => {return(
                                        <tr onClick={()=>{navigate("/offers-list", {state:{item:item, type:"OFFERS"}})}}>
                                            <td>
                                                <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                                                    <img src={item.picture?item.picture:"https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fuser.png?alt=media&token=10c0d83b-1a74-46a1-8d18-72dbb7b04cde"} alt={item.companyName} style={{width:50, height:50, borderRadius:50, marginRight:10}} />
                                                    <p>{item.name}</p>
                                                </div>
                                            </td>
                                            <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.usn}</div></td>
                                            <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.semester}</div></td>
                                            <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.placed?item.placed.length:"0"}</div></td>
                                        </tr>
                                    )})
                                }
                            </tbody>
                        </Table>
                    }
                </div>
            </div>
        </div>
    )
}

export default Students