import { child, get, ref } from "firebase/database";
import React, { useEffect, useState } from "react";
import { Form, Spinner, Table, Dropdown, Button } from "react-bootstrap";
import { firebaseDatabase } from "../../backend/firebase-handler";
import "./assessment-results.style.scss"
import exportFromJSON from "export-from-json";

const AssessmentResults = () => {

    const [courses, setCourses] = useState([])
    const [loading, setLoading] = useState(true)
    const [selected, setSelected] = useState("")
    const [resultList, setResultList] = useState([])
    const [originalList, setOriginalList] = useState([])
    const [departments, setDepartments] = useState(["All branches"])
    const [selectDept, setSelectedDept] = useState("All branches")
    const [colleges, setColleges] = useState(["All colleges"])
    const [selectedCollege, setSelectedCollege] = useState("All colleges")
    const [applyingFilter, setApplyingFilter] = useState(false)

    useEffect(() => {
        var tempAssmnt = []

        get(child(ref(firebaseDatabase), "ASSESSMENT_DETAILS")).then( async (snap) => {
            if (snap.exists()) {
                for (const key in snap.val()) {
                    tempAssmnt.push(snap.child(key).val())
                }
                await get(child(ref(firebaseDatabase), "PREQUALIFYING_EXAMS")).then((snap) => {
                    if (snap.exists()) {
                        for (const key in snap.val()) {
                            tempAssmnt.push(snap.child(key).val())
                        }
                        setLoading(false)
                        setCourses(tempAssmnt)
                    } else {
                        setLoading(false)
                    }
                })
            } else {
                setLoading(false)
            }
        })
    }, [])

    const handleSelect = async (value) => {
        var tempResult = []
        let tempDepartments = ["All branches"]
        let tempColleges = ["All colleges"]
        setLoading(true)
        await get(child(ref(firebaseDatabase), "ASSESSMENT_RESULTS")).then((snap) => {
            if (snap.exists()) {
                for (const id in snap.val()) {
                    if (snap.child(id).child(value).exists()) {
                        tempResult.push(snap.child(id).child(value).val())
                        if (!tempDepartments.includes(snap.child(id).child(value).child("student").child("department").val())) {
                            tempDepartments.push(snap.child(id).child(value).child("student").child("department").val())
                        }
                        if (!tempColleges.includes(snap.child(id).child(value).child("student").child("collegeName").val())) {
                            tempColleges.push(snap.child(id).child(value).child("student").child("collegeName").val())
                        }
                    }
                }
                // tempResult.sort((a,b) => (a.timeStamp > b.timeStamp) ? 1 : ((b.timeStamp > a.timeStamp) ? -1 : 0))
                setDepartments(tempDepartments)
                setColleges(tempColleges)
                tempResult.sort(compare)
                setResultList(tempResult)
                setOriginalList(tempResult)
                setSelectedCollege("All colleges")
                setSelectedDept("All branches")
                setLoading(false)
            } else {
                setLoading(false)
            }
        })
    }

    const exportAll = (range) => {
        if (resultList.length === 0) {
            alert("No data to exports")
        } else {
            const jsonArray = []
            const fileName = 'Assessment Scores-' + resultList[0].courseName     
            const exportType = 'csv'  
            for (const index in resultList) {
                if (range === "all") {
                    jsonArray.push({"Date":removeComma(reverseDate(resultList[index].date.split(" ")[0])), "Student Name":removeComma(resultList[index].student.name.toString()), USN:removeComma(resultList[index].student.usn.toString()), College:removeComma(resultList[index].student.collegeName.toString()), Department:removeComma(resultList[index].student.department.toString()), Semester:removeComma(resultList[index].student.semester.toString()), "Year of Graduation":removeComma(resultList[index].student.yearOfStudy.toString()), Percentage:removeComma(resultList[index].percentage.toString()), "Phone Number":removeComma(resultList[index].student.phoneNumber.toString()), "Email-ID":removeComma(resultList[index].student.emailId.toString())});
                } else {
                    if (resultList[index].badge === range) {
                        jsonArray.push({"Date":removeComma(reverseDate(resultList[index].date.split(" ")[0])), "Student Name":removeComma(resultList[index].student.name.toString()), USN:removeComma(resultList[index].student.usn.toString()), College:removeComma(resultList[index].student.collegeName.toString()), Department:removeComma(resultList[index].student.department.toString()), Semester:removeComma(resultList[index].student.semester.toString()), "Year of Graduation":removeComma(resultList[index].student.yearOfStudy.toString()), Percentage:removeComma(resultList[index].percentage.toString()), "Phone Number":removeComma(resultList[index].student.phoneNumber.toString()), "Email-ID":removeComma(resultList[index].student.emailId.toString())});
                    }
                }
            }
            exportFromJSON({ data:jsonArray, fileName, exportType })  
        }
    }

    const removeComma = (input) => {
        return input.split(",").join(" ");
    }
    const handleFilter = () => {
        let tempList = []

        if(selectedCollege === "All colleges" && selectDept === "All branches") {
            tempList = originalList
        } else if (selectedCollege === "All colleges" && selectDept !== "All branches") {
            for (const index in originalList) {
                if(originalList[index].student.department === selectDept) {
                    tempList.push(originalList[index])
                }
            } 
        } else if (selectedCollege !== "All colleges" && selectDept === "All branches") {
            for (const index in originalList) {
                if(originalList[index].student.collegeName === selectedCollege) {
                    tempList.push(originalList[index])
                }
            } 
        }
        else {
            for (const index in originalList) {
                if(originalList[index].student.collegeName === selectedCollege && originalList[index].student.department === selectDept) {
                    tempList.push(originalList[index])
                }
            } 
        }
        tempList.sort(compare)
        setResultList(tempList)
    }

    function compare( a, b ) {
        if ( a.timeStamp > b.timeStamp ){
          return -1;
        }
        if ( a.timeStamp < b.timeStamp ){
          return 1;
        }
        return 0;
    }

    const reverseDate = (date) => {
        var newDate = date.split("/")
        var temp = newDate[1] + "-" + newDate[0] + "-" + newDate[2]
        return temp
    }

    return(
        <div className="assessment-results-container">

            <div className="course-export-container">
                <Form.Group className="department-picker" controlId="exampleForm.ControlInput1">
                    <Form.Label className={'text-label'}>Select Course</Form.Label>
                    <Form.Select value={selected} onChange={event => {setSelected(event.target.value); handleSelect(event.target.value)}} className={'text-input picker-input'} aria-label="Default select example">
                        <option>Select course</option>
                        {
                            courses.map(item => <option key={item.id} value={item.id} >{item.courseName}</option>)
                        }
                    </Form.Select>
                </Form.Group>

                <Dropdown className="export-button">
                    <Dropdown.Toggle variant="primary" id="dropdown-basic">
                        Export score wise
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                        <Dropdown.Item onClick={()=>{exportAll("all")}}>All data</Dropdown.Item>
                        <Dropdown.Item onClick={()=>{exportAll("green")}}>90% - 100%</Dropdown.Item>
                        <Dropdown.Item onClick={()=>{exportAll("orange")}}>50% - 90%</Dropdown.Item>
                        <Dropdown.Item onClick={()=>{exportAll("red")}}>0% - 50%</Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
            </div>

            <p className="sub-title">Add filter for College/Department</p>
            
            <div className="picker-dropdown-container">
                <Form.Group className="department-picker" controlId="exampleForm.ControlInput1">
                    <Form.Label className={'text-label'}>Select College</Form.Label>
                    <Form.Select value={selectedCollege} onChange={event => {setSelectedCollege(event.target.value)}} className={'text-input picker-input'} aria-label="Default select example">
                        <option>Select department</option>
                        {
                            colleges.map(item => <option key={item} value={item} >{item}</option>)
                        }
                    </Form.Select>
                </Form.Group>
                <Form.Group className="department-picker" controlId="exampleForm.ControlInput1">
                    <Form.Label className={'text-label'}>Select Department</Form.Label>
                    <Form.Select value={selectDept} onChange={event => {setSelectedDept(event.target.value)}} className={'text-input picker-input'} aria-label="Default select example">
                        <option>Select department</option>
                        {
                            departments.map(item => <option key={item} value={item} >{item}</option>)
                        }
                    </Form.Select>
                </Form.Group>
                <Button disabled={applyingFilter} className="search-button" variant="primary" onClick={handleFilter}>Apply filter</Button>
            </div>

            {
                loading
                ?
                <Spinner animation="border" variant="primary" style={{marginTop:50, alignSelf:"center"}} />
                :
                resultList.length === 0
                ?
                <p style={{marginTop:50, marginBottom:20, alignSelf:"center", fontWeight:600, fontSize:16, color:"#ACACAC"}}>No one has taken this assessment yet!</p>
                :
                <Table hover className="table" responsive>
                    <thead>
                        <tr>
                                <th>Date</th>
                                <th>Name</th>
                                <th>USN</th>
                                <th>Department</th>
                                <th>College</th>
                                <th>Percentage</th>
                        </tr>
                    </thead>
                    <tbody className="table-body">
                        {
                            resultList.map((item, index) => {return(
                                <tr>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{reverseDate(item.date.split(" ")[0])}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.student.name}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.student.key}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.student.department}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.student.collegeName}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50, fontWeight:700, color:item.badge==="red"?"#FF8585":item.badge==="green"?"#82ECAC":"#FEE4A3"}}>{item.percentage}%</div></td>
                                </tr>
                            )})
                        }
                    </tbody>
                </Table>
            }
        </div>
    )
}

export default AssessmentResults