import { child, get, ref } from "firebase/database";
import React, { useEffect, useState } from "react";
import { Form, Spinner, Table } from "react-bootstrap";
import { firebaseDatabase } from "../../backend/firebase-handler";
import "./logs.style.scss"

const Logs = ({ type }) => {

    const [list, setList] = useState([])
    const [loading, setLoading] = useState(true)
    const [logList, setLogList] = useState([])
    const [selected, setSelected] = useState("")

    useEffect(() => {
        var temp = []

        get(child(ref(firebaseDatabase), "LOG/"+type)).then((snap)=>{
            if (snap.exists()) {
                for (const key in snap.val()) {
                    let tempObj = {name:snap.child(key).child("name").val(), id:key}
                    temp.push(tempObj)
                }
                setList(temp)
                setLoading(false)
            } else {
                setLoading(false)
            }
        })
    }, [])

    const handleSelect = async (id) => {
        let temp = []
        setLoading(false)
        await get(child(ref(firebaseDatabase), "LOG/"+type+"/"+id+"/log")).then((snap) => {
            if (snap.exists()) {
                for (const key in snap.val()) {
                    temp.push(snap.child(key).val())
                }
                setLogList(temp.reverse())
                setLoading(false)
            } else {
                setLoading(false)
            }
        })
    }

    return(
        <div className="logs-container">
            <Form.Group className="department-picker" controlId="exampleForm.ControlInput1">
                <Form.Label className={'text-label'}>Select {type==="COLLEGE"?"College":"Company"}</Form.Label>
                <Form.Select value={selected} onChange={event => {setSelected(event.target.value); handleSelect(event.target.value)}} className={'text-input picker-input'} aria-label="Default select example">
                    <option>Select {type==="COLLEGE"?"College":"Company"}</option>
                    {
                        list.map(item => <option key={item.id} value={item.id} >{item.name}</option>)
                    }
                </Form.Select>
            </Form.Group>

            {
                loading
                ?
                <Spinner animation="border" variant="primary" style={{marginTop:50, alignSelf:"center"}} />
                :
                logList.length === 0
                ?
                <p style={{marginTop:50, marginBottom:20, alignSelf:"center", fontWeight:600, fontSize:16, color:"#ACACAC"}}>No logs here!</p>
                :
                <Table hover className="table" responsive>
                    <thead>
                        <tr >
                                <th>Name</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Type</th>
                        </tr>
                    </thead>
                    <tbody className="table-body">
                        {
                            logList.map((item, index) => {return(
                                <tr>
                                    <td>
                                        <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                                            <img src={item.url} alt={item.name} style={{width:50, height:50, borderRadius:50, marginRight:10}} />
                                            <p>{item.name}</p>
                                        </div>
                                    </td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.date.split(" ")[0]}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.date.split(" ")[1]}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.type}</div></td>
                                </tr>
                            )})
                        }
                    </tbody>
                </Table>
            }
        </div>
    )
}

export default Logs