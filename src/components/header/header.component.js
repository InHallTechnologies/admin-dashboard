import React, { useContext, useState, useEffect} from "react";
import './header.style.scss'
import { Button } from 'react-bootstrap';
import { get, ref, child } from 'firebase/database'
import { firebaseDatabase } from "../../backend/firebase-handler";
import { RiAccountCircleFill } from 'react-icons/ri'

const Header = () => {

    const [planDetails, setPlanDetails] = useState({})

    useEffect(() => {
         
    }, [])

    return(
        <div className="header-container">
            <div className="name-plan-container">
                <p className="name">OccuHire Admin</p>
                <p className="plan">Tequed Labs Pvt. Ltd.</p>
            </div>
            <RiAccountCircleFill size={70} className="profile-picture" color="#52E1E2" />
        </div>
    )
}

export default Header