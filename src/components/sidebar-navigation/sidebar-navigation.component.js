import React, { useEffect, useState } from 'react';
import { MdSpaceDashboard, MdAddBox } from 'react-icons/md';
import { FaListUl, FaUniversity, FaUserGraduate } from 'react-icons/fa';
import { BiAddToQueue } from "react-icons/bi"
import { MdWork } from 'react-icons/md'
import { AiFillCode } from 'react-icons/ai'
import { IoStatsChart, IoDocumentText } from 'react-icons/io5'
import './sidebar-navigation.style.scss';
import { useLocation, useNavigate } from 'react-router-dom';
import logoSelected from "../../assets/sidebar_logo.png"
import logoUnSelected from "../../assets/sidebar_logo_grey.png"

const SidebarNavigation = ({onChange}) => {

    const location = useLocation();
    const navigation = useNavigate()
    const [selectedTab, setSelectedTab] = useState("Dashboard");

    useEffect(() => {
        if (location.search){
            const params = new URLSearchParams(location.search);
            setSelectedTab(params.get('tab'));
        }
        
    } , [])

    const handleChange = option => {
        setSelectedTab(option)
        navigation(`/?tab=${option}`)
        if (onChange) {
            onChange(option)
        }
    }

    return(
        <div className='sidebar-navigation-container'>
            <div className={selectedTab === "Dashboard"?'selected-option-container':'option-container'} onClick={_ => handleChange("Dashboard")} >
                <MdSpaceDashboard size={18} color={selectedTab === "Dashboard"?"#3761EE": "#C4C4C4"} />
                <h4 className='option-container-title' >Dashboard</h4>
            </div>

            <div className={selectedTab === "ActiveNewJobs"?'selected-option-container':'option-container'} onClick={_ => handleChange("ActiveNewJobs")} >
                <FaListUl size={18} color={selectedTab === "ActiveNewJobs"?"#3761EE": "#C4C4C4"} />
                <h4 className='option-container-title' >Active/New Jobs</h4>
            </div>

            <div className={selectedTab === "PostJob"?'selected-option-container':'option-container'} onClick={_ => handleChange("PostJob")} >
                <BiAddToQueue size={18} color={selectedTab === "PostJob"?"#3761EE": "#C4C4C4"} />
                <h4 className='option-container-title' >Post a Job</h4>
            </div>

            <div className={selectedTab === "StudentsData"?'selected-option-container':'option-container'} onClick={_ => handleChange("StudentsData")} >
                <FaUserGraduate size={18} color={selectedTab === "StudentsData"?"#3761EE": "#C4C4C4"} />
                <h4 className='option-container-title' >Students Data</h4>
            </div>

            <div className={selectedTab === "ManageCompanies"?'selected-option-container':'option-container'} onClick={_ => handleChange("ManageCompanies")} >
                <MdWork size={18} color={selectedTab === "ManageCompanies"?"#3761EE": "#C4C4C4"} />
                <h4 className='option-container-title' >Manage Companies</h4>
            </div>

            <div className={selectedTab === "Colleges"?'selected-option-container':'option-container'} onClick={_ => handleChange("Colleges")} >
                <FaUniversity size={18} color={selectedTab === "Colleges"?"#3761EE": "#C4C4C4"} />
                <h4 className='option-container-title' >Colleges</h4>
            </div>

            <div className={selectedTab === "PreQualifyingExams"?'selected-option-container':'option-container'} onClick={_ => handleChange("PreQualifyingExams")} >
                <AiFillCode size={18} color={selectedTab === "PreQualifyingExams"?"#3761EE": "#C4C4C4"} />
                <h4 className='option-container-title' >Pre-Qualifying Exams</h4>
            </div>

            <div className={selectedTab === "Assessments"?'selected-option-container':'option-container'} onClick={_ => handleChange("Assessments")} >
                <AiFillCode size={18} color={selectedTab === "Assessments"?"#3761EE": "#C4C4C4"} />
                <h4 className='option-container-title' >Assessments</h4>
            </div>

            <div className={selectedTab === "Reports"?'selected-option-container':'option-container'} onClick={_ => handleChange("Reports")} >
                <IoStatsChart size={18} color={selectedTab === "Reports"?"#3761EE": "#C4C4C4"} />
                <h4 className='option-container-title' >Reports/Stats</h4>
            </div>

            <div className={selectedTab === "Banners"?'selected-option-container':'option-container'} onClick={_ => handleChange("Banners")} >
                <img src={selectedTab==="DrivesByOccuHire"?logoSelected:logoUnSelected} alt="OccuHire" style={{width:23, height:23}} />
                <h4 className='option-container-title' >Banners/Logos</h4>
            </div>
        </div>
    )
}
export default SidebarNavigation;