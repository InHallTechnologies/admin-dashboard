import React, { useEffect, useState } from "react";
import "./student-banner.style.scss"
import { MdOutlineDeleteOutline } from 'react-icons/md'
import { firebaseDatabase, firebaseStorage } from '../../backend/firebase-handler'
import { child, get, ref, set } from 'firebase/database'
import { getDownloadURL, uploadBytesResumable, ref as firebaseStorageRef } from 'firebase/storage'
import { BsImage } from 'react-icons/bs'

const StudentBanner = () => {

    const [uploadingImage, setUploadingImage] = useState(false)
    const [studentImages, setStudentImages] = useState([])

    useEffect(() => {

        get(child(ref(firebaseDatabase), "CAROUSELS/STUDENT_DASHBOARD")).then((snap) => {
            if (snap.exists()) {
                setStudentImages(snap.val())
            }
        })
    }, [])

    const handleDelete = async (url) => {
        const newFilter = studentImages.filter((currentItem) => currentItem !== url)
        set(ref(firebaseDatabase, "CAROUSELS/STUDENT_DASHBOARD"), newFilter)
        setStudentImages(newFilter)
    }

    const pickImage = (index) => {
        const inputElement = document.createElement('input');
        inputElement.setAttribute('type',"file");
        inputElement.setAttribute('accept','image/*');
        inputElement.onchange = (event) => {
            const files = event.target.files;
            const FILE_NAME = Date.now() + "--STUDENT";
            const logoRef = firebaseStorageRef(firebaseStorage, "CAROUSELS/"+FILE_NAME);            
            const uploadTask = uploadBytesResumable(logoRef,files[0], {contentType:'image/png'}); 
            uploadTask.on('state_changed', (snapshot) => {
                setUploadingImage(true);
                const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                // setUploadPercentage(progress.toFixed(2));
            }, () => {
                // toast.error('Something went wrong');
                setUploadingImage(false);
            }, async () => {
                const url = await getDownloadURL(logoRef);
                studentImages[index] = url
                setStudentImages(studentImages)
                set(ref(firebaseDatabase, "CAROUSELS/STUDENT_DASHBOARD"), studentImages)
                setUploadingImage(false);
            })

        }
        inputElement.click();
    }   

    return(
        <div className='image-list-container'>
            <div className='add-title-container'>
                <p className="title">Add banner</p>
                <div onClick={()=>{if(!uploadingImage){pickImage(studentImages.length)}}} className="add-image-container">
                    <BsImage color='#ccc' size={30} />
                    <p className="add-image-tag">{uploadingImage?"Uploading a banner...":"Click to add image"}</p>
                </div>
            </div>
            
            {
                studentImages.map((item, index)=>{return(
                    <div className="single-banner-container">
                        <p className="title">Banner {index+1}</p>
                        <div className="add-banner-container">
                            {
                                <div className="banner-delete-container">
                                    <img src={item} alt="first banner" title="Click to change" onClick={()=>{pickImage(index)}} className="banner-image" />
                                    <div onClick={()=>{handleDelete(item)}} className="delete-container" >
                                        <MdOutlineDeleteOutline color="CF4539" size={20}  />
                                        <p className="delete">Delete Banner</p>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                )})
            }
        </div>
    )
}

export default StudentBanner