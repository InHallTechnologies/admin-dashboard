import React, { useEffect, useState } from "react";
import Header from "../header/header.component";
import './dashboard.style.scss'
import { Image, Button, Table, Row, Form } from 'react-bootstrap';
import { useNavigate } from "react-router-dom";
import { get, ref, child, query, limitToLast } from 'firebase/database'
import { firebaseDatabase } from "../../backend/firebase-handler";
import DatePicker from 'react-datepicker';
import departmentList from "../../entities/departmentList";
import "react-datepicker/dist/react-datepicker.css";

const Dashboard = () => {

    const [newDrives, setNewDrives] = useState([])
    const [activeDrives, setActiveDrives] = useState([])
    const [companies, setCompanies] = useState([])
    const [colleges, setColleges] = useState([])
    const [noOfStudents, setNoOfStudents] = useState("-")
    const [placed, setPlaced] = useState("-")
    const [unPlaced, setunPlaced] = useState("-")
    const [freePlan, setFreePlan] = useState("-")
    const [basic, setBasic] = useState("-")
    const [premium, setPremium] = useState("-")
    const navigation = useNavigate()
    const [startDate, setStartDate] = useState(new Date());
    const [selectedDepartment, setSelectedDepartment] = useState("");
    const [applyingFilter, setApplyingFilter] = useState(false)
    const [selectedCollege, setSelectedCollege] = useState("")
    const [branchList, setBranchList] = useState(["All Branches"])

    useEffect(() => {
        let tempNewDrives = []
        let tempActiveDrives = []
        let tempCompanies = []
        let tempColleges = []

        const queryNew = query(ref(firebaseDatabase, "BUFFER_VACANCY"), limitToLast(5))
        get(queryNew).then((snapShot) => {
            if (snapShot.exists()) {
                for (const key in snapShot.val()) {
                    tempNewDrives.push(snapShot.child(key).val())
                }
                setNewDrives(tempNewDrives.reverse())
            }
        })

        const queryActive = query(ref(firebaseDatabase, "APPROVED_VACANCY"), limitToLast(5))
        get(queryActive).then((snapShot) => {
            if (snapShot.exists()) {
                for (const key in snapShot.val()) {
                    tempActiveDrives.push(snapShot.child(key).val())
                }
                setActiveDrives(tempActiveDrives.reverse())
            }
        })

        const queryCompany = query(ref(firebaseDatabase, "COMPANY_ARCHIVE"), limitToLast(5))
        get(queryCompany).then((snapShot) => {
            if (snapShot.exists()) {
                for (const key in snapShot.val()) {
                    tempCompanies.push(snapShot.child(key).val())
                }
                setCompanies(tempCompanies.reverse())
            }
        })

        const queryCollege = query(ref(firebaseDatabase, "COLLEGE_ARCHIVE"), limitToLast(5))
        get(queryCollege).then((snapShot) => {
            if (snapShot.exists()) {
                for (const key in snapShot.val()) {
                    tempColleges.push(snapShot.child(key).val())
                }
                setColleges(tempColleges.reverse())
            }
        })

    }, [])

    const handleFilter = async () => {
        setApplyingFilter(true)
        let tempStudents = 0
        let placedTemp = 0
        let unPlacedTemp = 0
        let tempFree = 0
        let tempBasic = 0
        let tempPremium = 0
        if (selectedDepartment === "All Branches") {
            await get(child(ref(firebaseDatabase), "COLLEGE_WISE_STUDENTS/"+selectedCollege)).then((snap)=>{
                if (snap.exists()) {
                    for (const dept in snap.val()) {
                        for  (const key in snap.child(dept).val()) {
                            if (snap.child(dept).child(key).child("yearOfStudy").val() === startDate.getFullYear().toString()) {
                                tempStudents++
                                if (snap.child(dept).child(key).child("placed").exists()) {
                                    placedTemp++
                                } else {
                                    unPlacedTemp++
                                }
                                if (snap.child(dept).child(key).child("plan").val()==="Free Plan") {
                                    tempFree++   
                                } else if (snap.child(dept).child(key).child("plan").val()==="Basic Plan") {
                                    tempBasic++
                                } else {
                                    tempPremium++
                                }
                            }
                        }
                    }
                    setNoOfStudents(tempStudents)
                    setPlaced(placedTemp)
                    setunPlaced(unPlacedTemp)
                    setFreePlan(tempFree)
                    setBasic(tempBasic)
                    setPremium(tempPremium)
                    setApplyingFilter(false)
                } else {
                    setNoOfStudents(tempStudents)
                    setPlaced(placedTemp)
                    setunPlaced(unPlacedTemp)
                    setFreePlan(tempFree)
                    setBasic(tempBasic)
                    setPremium(tempPremium)
                    setApplyingFilter(false)
                }
            })
        } else {
            await get(child(ref(firebaseDatabase), "COLLEGE_WISE_STUDENTS/"+selectedCollege+"/"+selectedDepartment)).then((snap)=>{
                if (snap.exists()) {
                    for  (const key in snap.val()) {
                        if (snap.child(key).child("yearOfStudy").val() === startDate.getFullYear().toString()) {
                            tempStudents++
                            if (snap.child(key).child("placed").exists()) {
                                placedTemp++
                            } else {
                                unPlacedTemp++
                            }
                            if (snap.child(key).child("plan").val()==="Free Plan") {
                                tempFree++   
                            } else if (snap.child(key).child("plan").val()==="Basic Plan") {
                                tempBasic++
                            } else {
                                tempPremium++
                            }
                        }
                    }
                    setNoOfStudents(tempStudents)
                    setPlaced(placedTemp)
                    setunPlaced(unPlacedTemp)
                    setFreePlan(tempFree)
                    setBasic(tempBasic)
                    setPremium(tempPremium)
                    setApplyingFilter(false)
                } else {
                    setNoOfStudents(tempStudents)
                    setPlaced(placedTemp)
                    setunPlaced(unPlacedTemp)
                    setFreePlan(tempFree)
                    setBasic(tempBasic)
                    setPremium(tempPremium)
                    setApplyingFilter(false)
                }
            })
        }
    }

    const handleCollegeSelect = async (value) => {
        let tempList = ["All Branches"]

        await get(child(ref(firebaseDatabase), "COLLEGE_WISE_STUDENTS/"+value)).then((snap) => {
            if (snap.exists()) {
                for (const key in snap.val()) {
                    tempList.push(key)
                }
                setBranchList(tempList)
            }
        })
    }

    return(
        <div className="dashboard-container">
            {
                window.innerWidth >= 700
                &&
                <Header />
            }

            <div className="stats-filter-container">
                <div className="filter-container">
                    <Form.Group className="department-picker" controlId="exampleForm.ControlInput1">
                        <Form.Label className={'text-label'}>College</Form.Label>
                        <Form.Select value={selectedCollege} onChange={event => {setSelectedCollege(event.target.value); handleCollegeSelect(event.target.value)}} className={'text-input picker-input'} aria-label="Default select example">
                            <option>Select College</option>
                            {
                                colleges.map(item => <option key={item.uid} value={item.uid}>{item.collegeName}</option>)
                            }
                        </Form.Select>
                    </Form.Group>
                    <Form.Group className="department-picker" controlId="exampleForm.ControlInput1">
                        <Form.Label className={'text-label'}>Department</Form.Label>
                        <Form.Select value={selectedDepartment} onChange={event => setSelectedDepartment(event.target.value)} className={'text-input picker-input'} aria-label="Default select example">
                            <option>Select department</option>
                            {
                                branchList.map(item => <option key={item} >{item}</option>)
                            }
                        </Form.Select>
                    </Form.Group>
                    <Form.Group className="year-picker" controlId="exampleForm.ControlInput1">
                        <Form.Label className={'text-label'}>Graduation Year</Form.Label>
                        <DatePicker
                            selected={startDate}
                            onChange={(date) => setStartDate(date)}
                            showYearPicker
                            dateFormat="yyyy"
                            className='picker-input'
                        />
                    </Form.Group>
                    <Button disabled={applyingFilter} className="search-button" variant="primary" onClick={handleFilter}>Apply filter</Button>
                </div>
                <div className="stats-container-one">
                    <div className="tag-data-container">
                        <p className="tag">No of Students</p>
                        <p className="data">{noOfStudents.toString()}</p>
                    </div>
                    <div className="tag-data-container">
                        <p className="tag">Placed Students</p>
                        <p className="data">{placed.toString()}</p>
                    </div>
                    <div className="tag-data-container">
                        <p className="tag">Unplaced Students</p>
                        <p className="data">{unPlaced.toString()}</p>
                    </div>
                </div>
            </div>

            <div className="stats-container">
                <div className="tag-data-container">
                    <p className="tag">Free plan users</p>
                    <p className="data">{freePlan.toString()}</p>
                </div>
                <div className="tag-data-container">
                    <p className="tag">Basic plan users</p>
                    <p className="data">{basic.toString()}</p>
                </div>
                <div className="tag-data-container">
                    <p className="tag">Premium plan users</p>
                    <p className="data">{premium.toString()}</p>
                </div>
            </div>

            <div className="drives-container">
                <p className="section-title">Active drives</p>

                <Table hover className="table" responsive>
                    <thead>
                        <tr >
                            <th>Company</th>
                            <th>Role</th>
                            <th>Package</th>
                            <th>Location</th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody className="table-body">
                        {
                            activeDrives.map((item, index) => {return(
                                <tr>
                                    <td>
                                        <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                                            <img src={item.companyLogo} alt={item.companyName} style={{width:50, height:50, borderRadius:50, marginRight:10}} />
                                            <p>{item.companyName}</p>
                                        </div>
                                    </td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.role}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.startPackage}L - {item.endPackage}L</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.location}</div></td>
                                    <td><div style={{display:"flex", flexDirection:"row", alignItems:"center", height:50}}>
                                        <Button className="upgrade-button" variant="primary" onClick={()=>{navigation("/job-detail", {state:{item:item, type:"VIEW"}})}}>View/Edit</Button>
                                        <Button className="upgrade-button" style={{backgroundColor:"#52E1E2"}} variant="primary" onClick={()=>{navigation("/applications", {state:{id:item.jobId, company:item.companyUID, role:item.role, companyName:item.companyName}});}}>Application</Button>
                                    </div></td>
                                </tr>
                            )})
                        }
                    </tbody>
                </Table>
            </div>

            <div className="drives-container">
                <p className="section-title">New drives by companies</p>

                <Table hover className="table" responsive>
                    <thead>
                        <tr >
                            <th>Company</th>
                            <th>Role</th>
                            <th>Package</th>
                            <th>Location</th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody className="table-body">
                        {
                            newDrives.map((item, index) => {return(
                                <tr>
                                    <td>
                                        <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                                            <img src={item.companyLogo} alt={item.companyName} style={{width:50, height:50, borderRadius:50, marginRight:10}} />
                                            <p>{item.companyName}</p>
                                        </div>
                                    </td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.role}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.startPackage}L - {item.endPackage}L</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.location}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}><Button className="upgrade-button" variant="primary" onClick={()=>{navigation("/job-detail", {state:{item:item, type:"ACT"}})}}>Approve/Reject</Button></div></td>
                                </tr>
                            )})
                        }
                    </tbody>
                </Table>
            </div>

            <div className="drives-container">
                <p className="section-title">Recently on-boarded companies</p>

                <Table hover className="table" responsive>
                    <thead>
                        <tr >
                            <th>Company</th>
                            <th>HR Name</th>
                            <th>Phone Number</th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody className="table-body">
                        {
                            companies.map((item, index) => {return(
                                <tr>
                                    <td>
                                        <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                                            <img src={item.companyLogo?item.companyLogo:"https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fbuildings.png?alt=media&token=41b80aa2-3348-4616-8b04-5f155d9e0b55"} alt={item.companyName} style={{width:50, height:50, borderRadius:50, marginRight:10}} />
                                            <p>{item.companyName}</p>
                                        </div>
                                    </td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.hrName}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.hrContactNumber}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}><Button className="upgrade-button" variant="primary" onClick={()=>{navigation("/company-detail", {state:{item:item, type:"VIEW"}})}}>Details</Button></div></td>
                                </tr>
                            )})
                        }
                    </tbody>
                </Table>
            </div>

            <div className="drives-container">
                <p className="section-title">Recently on-boarded colleges</p>

                <Table hover className="table" responsive>
                    <thead>
                        <tr >
                            <th>College</th>
                            <th>PO Name</th>
                            <th>Phone Number</th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody className="table-body">
                        {
                            colleges.map((item, index) => {return(
                                <tr>
                                    <td>
                                        <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                                            <img src={item.companyLogo?item.companyLogo:"https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COMPANY_LOGO%2Fbuildings.png?alt=media&token=41b80aa2-3348-4616-8b04-5f155d9e0b55"} alt={item.companyName} style={{width:50, height:50, borderRadius:50, marginRight:10}} />
                                            <p>{item.collegeName}</p>
                                        </div>
                                    </td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.placementOfficerName}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.placementOfficerPhoneNumber}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}><Button className="upgrade-button" variant="primary" onClick={()=>{navigation("/college-detail", {state:item})}}>Details</Button></div></td>
                                </tr>
                            )})
                        }
                    </tbody>
                </Table>
            </div>
        </div>
    )
}

export default Dashboard