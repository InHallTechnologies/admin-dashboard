import React, { useEffect, useState } from "react";
import Header from "../header/header.component";
import "./assessment.style.scss"
import { get, ref, child } from 'firebase/database'
import { firebaseDatabase } from "../../backend/firebase-handler";
import { Spinner, Button } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

const Assessment = ({type}) => {

    const [assessmentsList, setAssessmentList] = useState([])
    const [laoding, setLoading] = useState(true)
    const navigation = useNavigate()

    useEffect(()=>{
        var temp = []
        if (type === "Pre Qualifying Exams") {
            get(child(ref(firebaseDatabase), "PREQUALIFYING_EXAMS")).then((snapShot)=>{
                if (snapShot.exists()) {
                    for (const key in snapShot.val()) {
                        temp.push(snapShot.child(key).val())
                    }
                    setAssessmentList(temp.reverse())
                    setLoading(false)
                } else {
                    setLoading(false)
                }
            })
        } else {
            get(child(ref(firebaseDatabase), "ASSESSMENT_DETAILS")).then((snapShot)=>{
                if (snapShot.exists()) {
                    for (const key in snapShot.val()) {
                        temp.push(snapShot.child(key).val())
                    }
                    setAssessmentList(temp.reverse())
                    setLoading(false)
                } else {
                    setLoading(false)
                }
            })
        }
        
    }, [])

    return(
        <div className="assessments-container">
            <Header />

            <div className="title-list-container">
                <div className="title-button-container">
                    <p className="section-title">{type} added by you</p>
                    <Button variant="primary" className="button" onClick={()=>{navigation("/edit-assessment", {state:{type:type}})}} >Add course</Button>
                </div>
                

                <div className="course-list-container">
                    {
                        laoding
                        ?
                        <Spinner animation="border" variant="primary" style={{marginTop:50, alignSelf:"center", marginLeft:20}} />
                        :
                        assessmentsList.length === 0
                        ?
                        <p style={{marginTop:50, alignSelf:"center", fontWeight:600, fontSize:16, color:"#ACACAC", marginLeft:20}}>No assessments added yet!</p>
                        :
                        assessmentsList.map((item, index)=>{return(
                            <div onClick={()=>{navigation('/edit-assessment',{state:{item:item, type:type}})}} className="course-container" >
                                <div className="logo-container">
                                    <img className="course-logo" src={item.courseLogo} alt="course-icon"  />
                                </div>
                                <p className="course-name">{item.courseName}</p>
                            </div>
                        )})
                    }
                </div>
            </div>
        </div>
    )
}

export default Assessment