import React, { useEffect, useState } from "react";
import Header from "../header/header.component";
import './colleges.style.scss'
import { BiSearchAlt } from 'react-icons/bi'
import { firebaseDatabase } from "../../backend/firebase-handler";
import { get, ref, child, query, limitToLast } from 'firebase/database'
import { Image, Button, Table, Row } from 'react-bootstrap';
import { useNavigate } from "react-router-dom";

const Colleges = () => {

    const [colleges, setColleges] = useState([])
    const [originalList, setOriginalList] = useState([])
    const [searchWord, setSearchWord] = useState("")
    const navigation = useNavigate()

    useEffect(() => {
        var temp = []

        get(child(ref(firebaseDatabase), "COLLEGE_ARCHIVE")).then((snapShot) => {
            if (snapShot.exists()) {
                for (const key in snapShot.val()) {
                    temp.push(snapShot.child(key).val())
                }
                setColleges(temp.reverse())
                setOriginalList(temp)
            }
        })
    }, [])

    const handleSearch = () => {
        var temp = []

        if (searchWord==="") {
            setColleges(originalList)
        } else {
            for (const index in originalList) {
                console.log(originalList[index])
                if (originalList[index].collegeName.toLowerCase().includes(searchWord.toLowerCase())) {
                    temp.push(originalList[index])
                }
            }
            setColleges(temp)
        }
    }

    return(
        <div className="colleges-container">
            <Header />

            <div className="search-container">
                <BiSearchAlt color="#ACACAC" size={18} />
                <input className="search-bar" placeholder="College Name" type="text" value={searchWord} onKeyUp={(event)=>{if(event.key === 'Enter' || event.keyCode === 13){handleSearch()}}} onChange={(event)=>{setSearchWord(event.target.value)}} />
            </div>

            <div className="college-list-container">
                <div className="title-button-container">
                    <p className="section-title">On-boarded colleges</p>
                    <Button variant="primary" className="button" onClick={()=>{navigation("/college-detail", {state:{type:"ADD"}})}} >Add college</Button>
                </div>

                <Table hover className="table" responsive>
                    <thead>
                        <tr >
                            <th>College</th>
                            <th>PO Name</th>
                            <th>Phone Number</th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody className="table-body">
                        {
                            colleges.map((item, index) => {return(
                                <tr>
                                    <td>
                                        <div style={{display:"flex", flexDirection:"row", alignItems:"center"}}>
                                            <img src={item.companyLogo?item.companyLogo:"https://firebasestorage.googleapis.com/v0/b/occuhire-tequed-labs.appspot.com/o/COLLEGE_LOGO%2Fcollege.png?alt=media&token=4b433e19-8007-4c8c-ad03-c6bd70d1f113"} alt={item.collegeName} style={{width:50, height:50, borderRadius:50, marginRight:10}} />
                                            <p>{item.collegeName}</p>
                                        </div>
                                    </td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.placementOfficerName}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}>{item.placementOfficerPhoneNumber}</div></td>
                                    <td><div style={{display:"flex", alignItems:"center", height:50}}><Button className="upgrade-button" variant="primary" onClick={()=>{navigation("/college-detail", {state:{item:item, type:"VIEW"}})}}>Detail</Button></div></td>
                                </tr>
                            )})
                        }
                    </tbody>
                </Table>
            </div>
        </div>
    )
}

export default Colleges